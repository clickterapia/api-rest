<?php
/**
 * Description of EventoAPI
 *
 * @author meza
 */
class EventoAPI extends EntityAPI {
    const GET_FINDXDATA = 'findxdata';
    const API_ACTION = 'evento';

    public function __construct() {
        $this->db = new EventoDB();
        $this->fields = [];
        array_push($this->fields, 
                'idsesion',
                'idpaciente',
                'idprofesional',
                'idtipoevento',
                'evento',
                'horaevento');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdPacienteEIdProfesional = isset($id) ? $id === self::GET_FINDXDATA : false;
        if($isByIdPacienteEIdProfesional) {
            $idpaciente = filter_input(INPUT_GET, 'fld1');
            $idprofesional = filter_input(INPUT_GET, 'fld2');
            $fecha= filter_input(INPUT_GET, 'fld3');
            $response = $this->db->getFindXdata($idpaciente, $idprofesional, $fecha);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idsesion, $obj->idpaciente, 
                $obj->idprofesional, $obj->idtipoevento, 
                $obj->evento, $obj->horaevento);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idsesion, $obj->idpaciente, 
                $obj->idprofesional, $obj->idtipoevento, 
                $obj->evento, $obj->horaevento);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}