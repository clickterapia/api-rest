<?php
/**
 * Description of BannerAPI
 *
 * @author meza
 */
class EncuestaTipoAPI extends EntityAPI {
    const API_ACTION = 'encuestatipo';

    public function __construct() {
        $this->db = new EncuestaTipoDB();
        $this->fields = [];
        array_push($this->fields, 
                'encuestatipo',
                'calificacion',
                'observaciones',
                'booleano');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $id = filter_input(INPUT_GET, 'id');
        $isUploadImagen = isset($id) ? $id === self::POST_IMAGEN : false;
        
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->titulo, $obj->fecinicio, 
                $obj->fecfin, $obj->imagenurl);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
     function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->titulo, $obj->fecinicio, 
                $obj->fecfin, $obj->imagenurl);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}
