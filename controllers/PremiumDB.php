<?php
/**
 * Description of PremiumDB
 *
 * @author meza
 */
class PremiumDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'premiums';
    
    public function getById($id=0){
        $query = "SELECT id, titulo, paquetepremium, 
                    fecinicio, fecfin , activo, 
                    duracion, costo, fecultmodif
                FROM premiums
                WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, titulo, paquetepremium, 
                    fecinicio, fecfin, activo, 
                    duracion, costo, fecultmodif 
                FROM premiums;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdProfesional($idProfesional=0){
        $query = "SELECT p.id, p.titulo, p.paquetepremium, 
                p.fecinicio, p.fecfin , p.activo, 
                p.duracion, p.costo, p.fecultmodif, 
                (CASE WHEN 
                    (SELECT x.idprofesional 
                    FROM premiumsxprofesionales x 
                    WHERE x.idpremium = p.id AND x.idprofesional = '$idProfesional')
                IS NOT NULL THEN 1 ELSE 0 END) AS contratado
            FROM premiums p
            WHERE p.activo = 1";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function insert( $titulo='', $paquetepremium='',  $fecinicio='', $fecfin='', $activo=-1,  $duracion=-1, $costo=-1){
        $query="INSERT INTO " . self::TABLE . " (
                titulo, paquetepremium, fecinicio, fecfin, activo, duracion, costo, fecultmodif) 
                VALUES (
                '$titulo', '$paquetepremium', '$fecinicio', '$fecfin', $activo, $duracion, $costo, NOW());";
        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $titulo='', $paquetepremium='', 
            $fecinicio='', $fecfin='', $activo=-1,  $duracion=-1, $costo=-1) {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     titulo = '$titulo', paquetepremium = '$paquetepremium', 
                     fecinicio = '$fecinicio', fecfin = '$fecfin', 
                     activo = $activo, duracion = $duracion, 
                     costo = $costo, 
                     fecultmodif = NOW() 
                     WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}