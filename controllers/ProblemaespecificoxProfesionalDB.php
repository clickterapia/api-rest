<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProblemaespecificoxProfesionalDB
 *
 * @author meza
 */
class ProblemaespecificoxProfesionalDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'problemasespecificosxprofesionales';
    
    public function getById($id=0){
        $query = "SELECT id, idproblemaespecifico, idprofesional
                 FROM problemasespecificosxprofesionales
                 WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, idproblemaespecifico, idprofesional
                FROM problemasespecificosxprofesionales;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( 
            $id='', $idproblemaespecifico=-1, $idprofesional='' ){
        $query="INSERT INTO " . self::TABLE . " (
                id, idproblemaespecifico, idprofesional, fecultmodif) 
                VALUES (
                '$id', $idproblemaespecifico, '$idprofesional', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update(
            $id='', $idproblemaespecifico=-1, $idprofesional='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     idproblemaespecifico = $idproblemaespecifico, idprofesional = '$idprofesional', fecultmodif = NOW() 
                     WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}