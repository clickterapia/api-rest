<?php
/**
 * Description of SesionAPI
 *
 * @author meza
 */
class SesionAPI extends EntityAPI {
    const GET_BYIDPACIENTEEIDPROFESIONAL = 'byidpacienteeidprofesional';
    const GET_RESERVASBYIDPROFESIONAL = 'reservasbyidprofesional';
    const GET_ESTADOCUENTABYIDPROFESIONAL = 'estadocuentabyidprofesional';
    const GET_BYIDPACIENTE = 'byidpaciente';
    const GET_SIGUIENTE = 'siguiente';
    const POST_VARIOS = 'varios';
    const API_ACTION = 'sesion';

    public function __construct() {
        $this->db = new SesionDB();
        $this->fields = [];
        array_push($this->fields, 
                'fecinicio',
                'fecfin',
                'iddisponibilidad',
                'idpaciente', 
                'observaciones',
                'precio');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdPacienteEIdProfesional = isset($id) ? $id === self::GET_BYIDPACIENTEEIDPROFESIONAL : false;
        $isReservasByIdProfesional = isset($id) ? $id === self::GET_RESERVASBYIDPROFESIONAL : false;
        $isEstadoCuentaByIdProfesional = isset($id) ? $id === self::GET_ESTADOCUENTABYIDPROFESIONAL : false;
        $isByIdPaciente = isset($id) ? $id === self::GET_BYIDPACIENTE : false;
        $isSiguiente = isset($id) ? $id === self::GET_SIGUIENTE : false;
        
        if ($isByIdPacienteEIdProfesional) {
            $idpaciente = filter_input(INPUT_GET, 'fld1');
            $idprofesional = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getByIdPacienteeIdProfesional($idpaciente, $idprofesional);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isReservasByIdProfesional) {
            $idprofesional = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getReservasByIdProfesional($idprofesional);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isEstadoCuentaByIdProfesional) {
            $idprofesional = filter_input(INPUT_GET, 'fld1');
            $fecinicio = filter_input(INPUT_GET, 'fld2');
            $fecfin = filter_input(INPUT_GET, 'fld3');
            $response = $this->db->getEstadoCuentaByIdProfesional($idprofesional, $fecinicio, $fecfin);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isByIdPaciente) {
            $idpaciente = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdPaciente($idpaciente);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isSiguiente) {
            $idpaciente = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getSiguiente($idpaciente);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        $isVarios = isset($id) ? $id === self::POST_VARIOS : false;
        if($isVarios) {
            $idpaciente = filter_input(INPUT_GET, 'fld1');
            $obj = json_decode(file_get_contents('php://input') );
            $r = $this->db->insertVarios($idpaciente, $obj);
            if ($r) {
                $this->response(200, "success", "Record updated");
            } else {
                $this->response(422, "error", "The property is not defined");
            }
            exit;
        }
        if(isset($obj->fecinicio) AND isset($obj->fecfin)
                AND isset($obj->iddisponibilidad) AND isset($obj->idpaciente)
                AND isset($obj->observaciones) AND isset($obj->precio)) {
            $r = $this->db->insert(
                    $obj->fecinicio, $obj->fecfin, $obj->iddisponibilidad,
                    $obj->idpaciente, $obj->observaciones, $obj->precio);
            if($r) { $this->response(200, "success", $r); }
        } else {
            $this->response(422, "error", "The property is not defined");
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if(!$id){
            $this->response(400, "error", "The id not defined");
            exit;
        }
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        if (empty($objArr)){                        
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        if(isset($obj->id) AND ($obj->fecinicio) AND isset($obj->fecfin)
                AND isset($obj->iddisponibilidad) AND isset($obj->idpaciente)
                AND isset($obj->observaciones) AND isset($obj->precio)){
            if($this->db->update(
                    $obj->id, $obj->fecinicio, $obj->fecfin, 
                    $obj->iddisponibilidad, $obj->idpaciente, $obj->observaciones,
                    $obj->precio)) {
                $this->response(200,"success","Record updated");                             
            } else {
                $this->response(304,"success","Record not updated");
            }
        }else{
            $this->response(422,"error","The property is not defined");                        
        }
    }
}