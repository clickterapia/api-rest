<?php
/**
 * Description of UsuarioApi
 *
 * @author WebDev
 */
class UsuarioAPI extends EntityAPI {
    const PUT_LOGIN = 'login';
    const API_ACTION = 'usuario';
    
    public function __construct() {
        $this->db = new UsuarioDB();
        $this->fields = [];
        array_push($this->fields, 
                'usuario',
                'contrasena',
                'apellido',
                'nombre');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if (isset($id)){
            $response = $this->db->getUsuario($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getUsuarios();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","No se enviaron datos.");
        } else if(isset($obj->auth)) {
            $response = $this->db->authenticate( $obj->auth->usuario,  $obj->auth->contrasena);
            if($response){
                $this->response(200,"success", $response[0]['id']);
            } else {
                $this->response(200,"error","Usuario o contraseña erróneos.");
            }
        } else if(isset($obj->apellido) AND isset($obj->nombre) 
                AND isset($obj->usuario) AND isset($obj->contrasena)) {
            $r = $this->db->insert($obj->apellido, $obj->nombre, 
                    $obj->usuario, $obj->contrasena);
            if($r) { $this->response(200,"success","Registro agregado con éxito."); }
        } else {
            $this->response(422,"error","Faltan datos obligatorios.");
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        
        $isLogin = isset($id) ? $id === self::PUT_LOGIN : false;

        if(!$id) {
            $this->response(422,"error","Id no enviado o erróneo.");
            exit;
        }
        if (empty($objArr)){                        
            $this->response(422,"error","No se enviaron datos.");
            exit;
        }
        if($isLogin) { 
            $r = $this->db->login($obj->usuario, $obj->contrasena);
            if($r != -1){ $this->response(200,"succes",$r); } 
            else { $this->response(244); }
            exit;
        }
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","Faltan campos");
            exit;
        } 
    }
}