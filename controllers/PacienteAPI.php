<?php
/**
 * Description of PacienteAPI
 *
 * @author meza
 */
class PacienteAPI extends EntityAPI {
    const GET_BYIDSOCIAL = 'byidsocial';
    const GET_BYIDPROFESIONAL = 'byidprofesional';
    const GET_BYIDEIDPROFESIONAL = 'byideidprofesional';
    const GET_CONSESIONES = 'consesiones';
    const GET_RESUMENES = 'resumenes';
    const PUT_HISTORIAL = 'historial';
    const PUT_LOGIN = 'login';
    const PUT_LOGINSOCIAL = 'loginsocial';
    const PUT_BLOQUEO = 'bloqueo';
    const API_ACTION = 'paciente';
    
    public function __construct() {
        $this->db = new PacienteDB();
        $this->fields = [];
        array_push($this->fields,
            'idsexo',
            'idpais', 
            'idprovincia',
            'fecnacimiento',
            'usuario', 
            'email',
            'contrasena',
            'notificacion', 
            'historialclinico',
            'fecregistro',
            'bloqueoadministrativo');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdSocial = isset($id) ? $id === self::GET_BYIDSOCIAL : false;
        $isByIdProfesional = isset($id) ? $id === self::GET_BYIDPROFESIONAL : false;
        $isByIdwIdProfesional = isset($id) ? $id === self::GET_BYIDEIDPROFESIONAL : false;
        $isConSesiones = isset($id) ? $id === self::GET_CONSESIONES : false;
        $isResumenes = isset($id) ? $id === self::GET_RESUMENES : false;
        
        if ($isByIdSocial) {
            $id = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdSocial($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isByIdProfesional) {
            $id = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdProfesional($id);
            echo json_encode($response, JSON_PRETTY_PRINT);
        } elseif($isByIdwIdProfesional) {
            $id = filter_input(INPUT_GET, 'fld1');
            $idprofesional = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getByIdeIdProfesional($id, $idprofesional);
            echo json_encode($response, JSON_PRETTY_PRINT);
        }  elseif($isConSesiones) {
            $response = $this->db->getConSesiones();
            echo json_encode($response, JSON_PRETTY_PRINT);
        } elseif ($isResumenes) {     
            $idpaciente = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getResumenes($idpaciente);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = (isset($obj->id)) ? $obj->id : '';
        $r = $this->db->insert($id,
                $obj->idsexo, $obj->idpais, $obj->idprovincia, 
                $obj->fecnacimiento, $obj->usuario, $obj->email, 
                $obj->contrasena, $obj->notificacion, $obj->historialclinico, 
                $obj->fecregistro, $obj->bloqueoadministrativo);
//        var_dump($r);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(205,$this->db->getLastError(),"No record added"); }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        $isHistorial = isset($id) ? $id === self::PUT_HISTORIAL : false;
        
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $isLogin = isset($id) ? $id === self::PUT_LOGIN : false;
        $isLoginSocial = isset($id) ? $id === self::PUT_LOGINSOCIAL : false;
        $isBloqueo = isset($id) ? $id === self::PUT_BLOQUEO : false;
        
        $obj = json_decode(file_get_contents('php://input') );
        
         if($isLogin) { 
            $r = $this->db->login($obj->usuario, $obj->contrasena);
            if($r != -1){ $this->response(200,"succes",$r); } 
            else { $this->response(204); }
            exit;
        } 
        
        if($isLoginSocial) { 
            $r = $this->db->loginSocial($obj->id, $obj->idsocial, $obj->fotourl);
            if($r !== -1){ $this->response(200,"success",$r); } 
            else { $this->response(204); }
            exit;
        } 
        
        if($isHistorial) {
            $id = filter_input(INPUT_GET, 'fld1');
            $r = $this->db->updateHistorial($id, $obj->historialclinico);
            if($r) { $this->response(200,"success","Record updated"); }
            else { $this->response(204,"success","Record not updated");}
            exit;
        }
        
         if($isBloqueo) { 
            $r = $this->db->updateBloqueo($obj->id, $obj->bloqueoadministrativo);
            if($r !== -1){ $this->response(200,"success",$r); } 
            else { $this->response(204); }
            exit;
        } 
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idsexo, $obj->idpais, 
                $obj->idprovincia, $obj->fecnacimiento, $obj->usuario, 
                $obj->email, $obj->contrasena, $obj->notificacion,
                $obj->historialclinico, $obj->fecregistro, $obj->bloqueoadministrativo);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}