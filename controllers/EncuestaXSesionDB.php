<?php
/**
 * Description of EncuestaxSesionDB
 *
 * @author meza
 */
class EncuestaXSesionDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'encuestasxsesiones';
    
    public function getById($id=0){
        $query = "SELECT e.id, e.idsesion, e.idencuesta, n.encuesta, 
                e.calificacion, e.observaciones, e.valorbooleano,
                p.usuario AS paciente, CONCAT(f.apellido, ', ', f.nombre) AS profesional,
                s.fecinicio AS fecha, t.encuestatipo
            FROM encuestasxsesiones e 
            LEFT JOIN encuestas n ON n.id = e.idencuesta
            LEFT JOIN encuestastipos t ON t.id = n.idencuestatipo
            LEFT JOIN sesiones s ON s.id = e.idsesion
            LEFT JOIN pacientes p ON p.id = s.idpaciente
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN profesionales f ON f.id = d.idprofesional
            WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT e.id, e.idsesion, e.idencuesta, n.encuesta, 
                e.calificacion, e.observaciones, e.valorbooleano,
                p.usuario AS paciente, CONCAT(f.apellido, ', ', f.nombre) AS profesional,
                s.fecinicio AS fecha, t.encuestatipo, e.publicar
            FROM encuestasxsesiones e 
            LEFT JOIN encuestas n ON n.id = e.idencuesta
            LEFT JOIN encuestastipos t ON t.id = n.idencuestatipo
            LEFT JOIN sesiones s ON s.id = e.idsesion
            LEFT JOIN pacientes p ON p.id = s.idpaciente
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN profesionales f ON f.id = d.idprofesional;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idsesion='', $idencuesta='',  $calificacion=-1, 
            $valorbooleano=0, $observaciones=''){
        $id = self::gen_uuid();
        $query="INSERT INTO " . self::TABLE . " (id,
                idsesion, idencuesta, calificacion, 
                valorbooleano, observaciones, publicar, fecultmodif) 
            VALUES ('$id',
                '$idsesion', '$idencuesta', $calificacion,
                $valorbooleano, '$observaciones', 0, NOW());";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        return $id;
    }
    
    public function update($id='', 
            $idsesion='', $idencuesta='', $calificacion=-1, 
            $valorbooleano=0, $observaciones='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                idsesion = '$idsesion', idencuesta = '$idencuesta', calificacion = $calificacion, 
                valorbooleano = $valorbooleano, observaciones = '$observaciones', fecultmodif = NOW() 
                WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function publicar($id='') {
        $query = "UPDATE " . self::TABLE . " SET 
            publicar = CASE WHEN publicar = 0 THEN 1 ELSE 0 END, 
            fecultmodif = NOW() 
            WHERE id = '$id';";
//          var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}