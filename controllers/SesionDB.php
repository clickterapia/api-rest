<?php
/**
 * Description of SesionDB
 *
 * @author meza
 */
class SesionDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'sesiones';
    
    public function getById($id=0){
        $query = "SELECT s.id, s.fecinicio, s.fecfin, 
                s.iddisponibilidad, d.idprofesional, p.titulo, p.nombre, 
                p.apellido, s.idpaciente, a.usuario, s.observaciones, s.precio
            FROM sesiones s
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN profesionales p ON p.id = d.idprofesional
            LEFT JOIN pacientes a ON a.id = s.idpaciente
            WHERE s.id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT s.id, s.fecinicio, s.fecfin, 
                    s.iddisponibilidad, d.idprofesional, p.nombre, p.apellido, 
                    s.idpaciente, a.usuario, s.observaciones, s.precio,
                    CONCAT(p.apellido, ', ', p.nombre) AS profesional
                 FROM sesiones s
                 LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
                 LEFT JOIN profesionales p ON p.id = d.idprofesional
                 LEFT JOIN pacientes a ON a.id = s.idpaciente;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdPacienteeIdProfesional($idpaciente='', $idprofesional=''){
        $query = "SELECT s.id, s.fecinicio, s.fecfin, 
                    s.iddisponibilidad, d.idprofesional, p.nombre, p.apellido, 
                    s.idpaciente, a.usuario, s.observaciones, s.precio
                 FROM sesiones s
                 LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
                 LEFT JOIN profesionales p ON p.id = d.idprofesional
                 LEFT JOIN pacientes a ON a.id = s.idpaciente
                 WHERE a.id = '$idpaciente' AND p.id = '$idprofesional';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getReservasByIdProfesional($idprofesional=''){
        $query = "SELECT s.id, s.fecinicio, s.fecfin, 
                s.iddisponibilidad, d.idprofesional, p.nombre, p.apellido, 
                s.idpaciente, a.usuario, s.observaciones, s.precio,
                (SELECT MAX(s1.fecinicio)
                FROM sesiones s1 
                LEFT JOIN disponibilidades d1 ON d1.id = s1.iddisponibilidad
                LEFT JOIN profesionales p1 ON p1.id = d1.idprofesional
                WHERE p1.id = '$idprofesional' AND s1.idpaciente = s.idpaciente
                AND s1.fecfin < NOW()) AS ultima
            FROM sesiones s
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN profesionales p ON p.id = d.idprofesional
            LEFT JOIN pacientes a ON a.id = s.idpaciente
            WHERE p.id = '$idprofesional' AND (s.fecinicio >= NOW() OR s.fecfin > NOW())
			ORDER BY s.fecinicio;";
       // var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getEstadoCuentaByIdProfesional($idprofesional='', $fecinicio='', $fecfin='') {
        $query = "SELECT d.idprofesional, '$fecinicio' AS fecinicio, '$fecfin' AS fecfin, 
                COUNT(d.idprofesional) AS cantidad, SUM(s.precio) AS monto
            FROM sesiones s
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            WHERE d.idprofesional = '$idprofesional' AND s.fecinicio >= '$fecinicio' AND s.fecfin <= '$fecfin';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getByIdPaciente($idpaciente=''){
        $query = "SELECT s.id, s.fecinicio, s.fecfin, 
                s.iddisponibilidad, d.idprofesional, p.nombre, p.apellido, 
                s.idpaciente, a.usuario, s.observaciones, s.precio, 
                CONCAT(p.titulo, ' ', p.apellido, ', ', p.nombre) AS profesional
            FROM sesiones s
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN profesionales p ON p.id = d.idprofesional
            LEFT JOIN pacientes a ON a.id = s.idpaciente
            WHERE a.id = '$idpaciente';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getSiguiente($idpaciente=''){
        $query = "SELECT s.id, s.fecinicio, s.fecfin, 
                s.iddisponibilidad, d.idprofesional, p.titulo, p.nombre, 
                p.apellido, s.idpaciente, a.usuario, s.observaciones, s.precio
            FROM sesiones s
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN profesionales p ON p.id = d.idprofesional
            LEFT JOIN pacientes a ON a.id = s.idpaciente
            WHERE s.idpaciente = '$idpaciente' AND (s.fecinicio >= NOW() OR s.fecfin > NOW())
            ORDER BY s.fecinicio ASC LIMIT 1;";
       // var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function insert( 
            $fecinicio='', $fecfin='', $iddisponibilidad='',
            $idpaciente='', $observaciones='', $precio=-1){
        $returnvalue = 0;
        $stmt = $this->mysqli->prepare("SET @p_newid:= -1");
        $stmt->execute();
        $query = "CALL sp_abm_reserva(-1, '$idpaciente', '$iddisponibilidad', 
                '$fecinicio', '$fecfin', '$observaciones',
                $precio, 0, 0, @p_newid);";
//            var_dump($query);
        $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_newid as p_newid');
        $row = $r->fetch_assoc();
        $returnvalue+=$row['p_newid'];
        return true;
//        $query="INSERT INTO " . self::TABLE . " (
//                id, fecinicio, fecfin, 
//                iddisponibilidad, idpaciente, observaciones, 
//                precio, fecultmodif) 
//                VALUES (
//                '$id', '$fecinicio', '$fecfin', '$iddisponibilidad', 
//                '$idpaciente', '$observaciones', $precio, NOW());";
////        var_dump($query);
//        $stmt = $this->mysqli->prepare($query);
//        $stmt->execute();        
//        $close = $stmt->close();
//        $lastid = $this->mysqli->insert_id;
//        return $lastid;
    }
    
    public function insertVarios(
            $idpaciente='', $events=null) {
        // elimino todos los eventos
//        $query = "DELETE FROM sesiones 
//            WHERE idpaciente = '$idpaciente'
//                AND pagado = 0 AND realizado = 0
//                AND fecinicio > NOW();";
//        $stmt = $this->mysqli->prepare($query);
//
//        $r = $stmt->execute(); 
//        $stmt->close();
//
//        if(count($events) === 0) {
//            return true;
//        }
        $returnvalue = 0;
        foreach($events as $event){
            $stmt = $this->mysqli->prepare("SET @p_newid:= -1");
            $stmt->execute();
            $query = "CALL sp_abm_reserva(-1, '$idpaciente', '$event->iddisponibilidad', 
                    '$event->fecinicio', '$event->fecfin', '$event->observaciones',
                    $event->precio, 0, 0, @p_newid);";
//            var_dump($query);
            $this->mysqli->query($query);
            $r = $this->mysqli->query('SELECT @p_newid as p_newid');
            $row = $r->fetch_assoc();
            $returnvalue+=$row['p_newid'];
        }
        if ($returnvalue != count($events)) {
            return false;
        }
        return true;
    }
    
    public function insert2(
            $id='', $idpaciente='', $iddisponibilidad='', 
            $fecinicio='', $fecfin='', $observaciones='',
            $precio=0, $pagado=0, $realizado=0){
        $query="INSERT INTO sesiones (
                id, fecinicio, fecfin, 
                iddisponibilidad, idpaciente, observaciones, 
                precio, fecultmodif) 
                VALUES (
                '$id', '$fecinicio', '$fecfin', '$iddisponibilidad', 
                '$idpaciente', '$observaciones', $precio, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update( 
            $id='', $fecinicio='', $fecfin='', 
            $iddisponibilidad='', $idpaciente='', $observaciones='',
            $precio=-1) {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     fecinicio = '$fecinicio', fecfin = '$fecfin', 
                     iddisponibilidad = '$iddisponibilidad', idpaciente = '$idpaciente', 
                     observaciones = '$observaciones', precio = $precio, fecultmodif = NOW() 
                     WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}