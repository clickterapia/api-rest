<?php
/**
 * Description of UsuarioDb
 *
 * @author WebDev
 */
class UsuarioDB extends EntityDB {
   const TABLE = 'usuarios';
   protected $mysqli;
    
    public function getUsuario($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM usuarios WHERE id=$id;");
        $stmt->execute();
        $result = $stmt->get_result();
        $usuario = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $usuario;
    }
    
    public function getUsuarios(){
        $result = $this->mysqli->query('SELECT * FROM usuarios');
        $usuarios = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $usuarios;
    }
    
    public function insert($apellido='', $nombre='', $usuario='', $contrasena=''){
        $query = "INSERT INTO " . self::TABLE . " (apellido, nombre, usuario, contrasena, fecultmodif) "
                . "VALUES ('$apellido', '$nombre', '$usuario', '$contrasena', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id, $apellido, $nombre, 
            $usuario, $contrasena) {
        $query = "UPDATE usuarios SET apellido='$apellido', nombre='$nombre', "
                . "usuario='$usuario', contrasena='$contrasena' WHERE id = $id;";
//        var_dump($query);
        if($this->checkIntID( self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function login($usuario='', $contrasena=''){
        $query = "SELECT id FROM usuarios WHERE usuario='$usuario' AND contrasena='$contrasena'";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $r = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return (count($r)>0) ? $r : -1;
    }
    
    public function resetPass($id, $contrasena) {        
        $query = "UPDATE usuarios SET contrasena = '$contrasena' WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function changePass($id, $newcontrasena) {        
        $query = "UPDATE usuarios SET contrasena = '$newcontrasena' WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
//    public function authenticate($usuario='', $contrasena=''){
//        $query = "SELECT id FROM usuarios WHERE usuario='$usuario' AND contrasena='$contrasena'";
//        var_dump($query);
//        $stmt = $this->mysqli->prepare($query);
//        $stmt->execute();
//        $result = $stmt->get_result();
//        $usuario = $result->fetch_all(MYSQLI_ASSOC);
//        $stmt->close();
//        
//        return $usuario;
//    }
}