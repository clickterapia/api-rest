<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProblemaAPI
 *
 * @author meza
 */
class ProblemaAPI extends EntityAPI {
    const GET_BYIDSESION = 'byidsesion';
    const GET_NOTRATADOS = 'notratados';
    const GET_SANCIONESBYID = 'sancionesbyid';
    const API_ACTION = 'problema';

    public function __construct() {
        $this->db = new ProblemaDB();
        $this->fields = [];
        array_push($this->fields, 
                'idsesion',
                'idorigen',
                'tipoorigen',
                'idproblematipo',
                'idproblemaestado',
                'problema',
                'acciones');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdSesion = isset($id) ? $id === self::GET_BYIDSESION : false;
        $isNoTratados = isset($id) ? $id === self::GET_NOTRATADOS : false;
        $isSanciones = isset($id) ? $id === self::GET_SANCIONESBYID : false;
        
        if ($isByIdSesion) {
            $idsesion = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdSesion($idsesion);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isNoTratados) {
            $response = $this->db->getNoTratados();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isSanciones) {
            $idprofesional = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getSancionesByIdProfesional($idprofesional);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idsesion, $obj->idorigen, 
                $obj->tipoorigen, $obj->idproblematipo,
                $obj->problema, $obj->acciones, $obj->idproblemaestado);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idsesion, $obj->idorigen, 
                $obj->tipoorigen, $obj->idproblematipo,
                $obj->problema, $obj->acciones, $obj->idproblemaestado);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}