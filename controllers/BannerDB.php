<?php
/**
 * Description of BannerDB
 *
 * @author meza
 */
class BannerDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'banners';
    
    public function getById($id=0){
        $query = "SELECT id, titulo, fecinicio, fecfin, imagenurl 
                 FROM banners
                 WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, titulo, fecinicio, fecfin, imagenurl 
                FROM banners;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getListForPacientes() {
        $query = "SELECT id, titulo, fecinicio, fecfin, imagenurl 
            FROM banners
            WHERE NOW() BETWEEN fecinicio AND fecfin;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }


    public function insert( $titulo='', $fecinicio='',  $fecfin='', $imagenurl=''){
        $query="INSERT INTO " . self::TABLE . " (
                titulo, fecinicio, fecfin, imagenurl, fecultmodif) 
                VALUES (
                '$titulo', '$fecinicio', '$fecfin', '$imagenurl', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $titulo='', $fecinicio='', 
            $fecfin='', $imagenurl='') {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     titulo = '$titulo', fecinicio = '$fecinicio', 
                     fecfin = '$fecfin', imagenurl = '$imagenurl', 
                     fecultmodif = NOW() 
                     WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function uploadFileImagen($id='', $filePath='') {
        $imagenurl = $this->rootPath . $filePath;
        $query="UPDATE " . self::TABLE . " SET
                imagenurl='$imagenurl',  
                fecultmodif=NOW()
                WHERE id='$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
