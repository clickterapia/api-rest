<?php
/**
 * Description of ProblemaDB
 *
 * @author meza
 */
class ProblemaDB extends EntityDB {
    const ESTADO_PENDIENTE = '1';
    const ESTADO_REVISADO = '2';
    const ESTADO_TRATADO = '3';
    protected $mysqli;
    const TABLE = 'problemas';
    
    public function getById($id=0){
        $query = "SELECT id, idsesion, idorigen, 
                    tipoorigen, idproblematipo, problema, 
                    acciones, idproblemaestado
                 FROM problemas
                 WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT p.id, p.idsesion, s.fecinicio, p.idorigen, p.tipoorigen, 
                (CASE  
                    WHEN tipoorigen = 0 THEN 'Administracion'
                    WHEN tipoorigen = 1 THEN (SELECT CONCAT(r.apellido, ', ', r.nombre) FROM profesionales r WHERE r.id = idorigen)
                    WHEN tipoorigen = 2 THEN (SELECT a.usuario FROM pacientes a WHERE a.id = idorigen)
                END) AS origen, 
                p.idproblematipo, t.problematipo, p.problema, 
                p.acciones, p.idproblemaestado, e.problemaestado
            FROM problemas p
            LEFT JOIN sesiones s ON s.id = p.idsesion
            LEFT JOIN problemastipos t ON t.id = p.idproblematipo
            LEFT JOIN problemasestados e ON e.id = p.idproblemaestado;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getNoTratados() {
        $query = "SELECT p.id, p.idsesion, s.fecinicio, p.idorigen, p.tipoorigen, 
                (CASE  
                    WHEN tipoorigen = 0 THEN 'Administracion'
                    WHEN tipoorigen = 1 THEN 'Profesional'
                    WHEN tipoorigen = 2 THEN 'Paciente'
                END) AS origen, 
                (CASE  
                    WHEN tipoorigen = 0 THEN 'Administracion'
                    WHEN tipoorigen = 1 THEN (SELECT CONCAT(r.apellido, ', ', r.nombre) FROM profesionales r WHERE r.id = p.idorigen)
                    WHEN tipoorigen = 2 THEN (SELECT a.usuario FROM pacientes a WHERE a.id = idorigen)
                END) AS emisor, 
                p.idproblematipo, t.problematipo, p.problema, 
                p.acciones, p.idproblemaestado, e.problemaestado, p.fecultmodif,
                s.idpaciente, c.usuario AS paciente, d.idprofesional, 
                CONCAT(f.apellido, ', ', f.nombre) AS profesional
            FROM problemas p
            LEFT JOIN sesiones s ON s.id = p.idsesion
            LEFT JOIN problemastipos t ON t.id = p.idproblematipo
            LEFT JOIN problemasestados e ON e.id = p.idproblemaestado
            LEFT JOIN pacientes c ON c.id = s.idpaciente
            LEFT JOIN disponibilidades d ON d.id  = s.iddisponibilidad
            LEFT JOIN profesionales f ON f.id = d.idprofesional
            WHERE p.idproblemaestado <> " . self::ESTADO_TRATADO . ";";
        
//        var_dump($query);
//        return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdSesion($idsesion='') {
        $query = "SELECT p.id, p.idsesion, p.idorigen, p.tipoorigen, 
                (CASE  
                    WHEN p.tipoorigen = 0 THEN 'Administracion'
                    WHEN p.tipoorigen = 1 THEN (SELECT CONCAT(r.apellido, ', ', r.nombre) FROM profesionales r WHERE r.id = p.idorigen)
                    WHEN p.tipoorigen = 2 THEN (SELECT a.usuario FROM pacientes a WHERE a.id = p.idorigen)
                END) AS origen, p.idproblematipo, t.problematipo, p.idproblemaestado, 
                e.problemaestado, p.problema, p.acciones
            FROM problemas p
            LEFT JOIN sesiones s ON s.id = p.idsesion
            LEFT JOIN problemastipos t ON t.id = p.idproblematipo
            LEFT JOIN problemasestados e ON e.id = p.idproblemaestado
            WHERE p.idsesion = '$idsesion';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getSancionesByIdProfesional($idprofesional='') {
        $query = "SELECT p.id, p.idsesion, s.fecinicio, p.idorigen, p.tipoorigen, 
                (CASE  
                    WHEN tipoorigen = 0 THEN 'Administracion'
                    WHEN tipoorigen = 1 THEN 'Profesional'
                    WHEN tipoorigen = 2 THEN 'Paciente'
                END) AS origen, 
                (CASE  
                    WHEN tipoorigen = 0 THEN 'Administracion'
                    WHEN tipoorigen = 1 THEN (SELECT CONCAT(r.apellido, ', ', r.nombre) FROM profesionales r WHERE r.id = p.idorigen)
                    WHEN tipoorigen = 2 THEN (SELECT a.usuario FROM pacientes a WHERE a.id = idorigen)
                END) AS emisor, 
                p.idproblematipo, t.problematipo, p.problema, 
                p.acciones, p.idproblemaestado, e.problemaestado, p.fecultmodif,
                s.idpaciente, c.usuario AS paciente, d.idprofesional, 
                CONCAT(f.apellido, ', ', f.nombre) AS profesional
            FROM problemas p
            LEFT JOIN sesiones s ON s.id = p.idsesion
            LEFT JOIN problemastipos t ON t.id = p.idproblematipo
            LEFT JOIN problemasestados e ON e.id = p.idproblemaestado
            LEFT JOIN pacientes c ON c.id = s.idpaciente
            LEFT JOIN disponibilidades d ON d.id  = s.iddisponibilidad
            LEFT JOIN profesionales f ON f.id = d.idprofesional
            WHERE t.ausencia = 1 AND d.idprofesional = '$idprofesional';";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function insert( 
            $id='', $idsesion='', $idorigen='', 
            $tipoorigen=-1, $idproblematipo=-1, $problema='',
            $acciones='', $idproblemaestado=-1){
        $query="INSERT INTO " . self::TABLE . " (
                id, idsesion, idorigen, tipoorigen, 
                idproblematipo, problema, acciones, 
                idproblemaestado, fecultmodif) 
                VALUES (
                '$id', '$idsesion', '$idorigen', $tipoorigen, 
                $idproblematipo, '$problema', '$acciones',
                 $idproblemaestado, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update(
            $id='', $idsesion='', $idorigen='', 
            $tipoorigen=-1, $idproblematipo=-1, $problema='',
            $acciones='', $idproblemaestado=-1) {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     idsesion = '$idsesion', idorigen = '$idorigen', 
                     tipoorigen = $tipoorigen, idproblematipo = $idproblematipo, 
                     problema = '$problema', acciones = '$acciones', 
                     idproblemaestado = $idproblemaestado, fecultmodif = NOW() 
                     WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}