<?php
/**
 * Description of BannerAPI
 *
 * @author meza
 */
class BannerAPI extends EntityAPI {
    const GET_LISTFORPACIENTES = 'listforpacientes';
    const POST_IMAGEN = 'imagen';
    const API_ACTION = 'banner';

    public function __construct() {
        $this->db = new BannerDB();
        $this->fields = [];
        array_push($this->fields, 
                'titulo',
                'fecinicio',
                'fecfin',
                'imagenurl');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isListForPacientes = isset($id) ? $id === self::GET_LISTFORPACIENTES : false;
        
        if($isListForPacientes) {
            $response = $this->db->getListForPacientes();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $id = filter_input(INPUT_GET, 'id');
        $isUploadImagen = isset($id) ? $id === self::POST_IMAGEN : false;
        
        if ($isUploadImagen) {
            $id = filter_input(INPUT_GET, 'fld1');
            $filePath = $r = $this->uploadFile($id, 'banner/', 'banner_');
            if($filePath === 500) {
                $this->response(500,"false", "Directorio destino no habilitado para escritura");
                exit;
            } else if($filePath === 501) {
                $this->response(500, "false", "Archivo no subido!!");
                exit;
            }
            $r = $this->db->uploadFileImagen($id, $filePath);
            if($r){ $this->response(200,"succes",$r); } 
            else { $this->response(204); }
            exit;
        }
        
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->titulo, $obj->fecinicio, 
                $obj->fecfin, $obj->imagenurl);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
     function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->titulo, $obj->fecinicio, 
                $obj->fecfin, $obj->imagenurl);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
    
    function uploadFile($id = '', $dir='', $prefix='') {
        if (isset($_FILES['file'])) {
            $rootPath = $this->getRootPath();
            $originalName = $_FILES['file']['name'];
            $ext = '.' . pathinfo($originalName, PATHINFO_EXTENSION);
            $generatedName = $dir . $prefix . $id . $ext;
            $filePath = $rootPath . $generatedName;

            if (!is_writable($rootPath)) {
                return 500;
            }

            if (move_uploaded_file($_FILES['file']['tmp_name'], $filePath)) {
                return $generatedName;
            }
        }
        else {
            return 501;
        }
    }
    
    function getRootPath() {
        $ini = parse_ini_file('conf.ini', true);
        if($ini['enviroment']['prod']==1) {
            $conf=$ini['prod'];
        } else {
            $conf=$ini['debug'];
        }
        return $conf['imagestock'];
    }
}
