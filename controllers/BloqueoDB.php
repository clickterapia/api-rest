<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  Tipobloqueante si es 0 es administracion, si es 1 es profesional y si es 2 paciente
 */

/**
 * Description of BloqueoDB
 *
 * @author meza
 */
class BloqueoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'bloqueos';
    
    public function getById($id=0){
        $query = "SELECT id, idbloqueante, idbloqueado, tipobloqueante, tipobloqueado, razon, fechahora 
                 FROM bloqueos
                 WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, idbloqueante, idbloqueado, tipobloqueante, 
                (CASE  
                        WHEN tipobloqueante = 0 THEN 'Administracion'
                        WHEN tipobloqueante = 1 THEN (SELECT CONCAT(r.apellido, ', ', r.nombre) FROM profesionales r WHERE r.id = idbloqueante)
                        WHEN tipobloqueante = 2 THEN (SELECT a.usuario FROM pacientes a WHERE a.id = idbloqueante)
                END) AS bloqueante, 
                tipobloqueado,
                (CASE 
                        WHEN tipobloqueado = 0 THEN 'Administracion'
                        WHEN tipobloqueado = 1 THEN (SELECT CONCAT(r.apellido, ', ', r.nombre) FROM profesionales r WHERE r.id = idbloqueado)
                        WHEN tipobloqueado = 2 THEN (SELECT a.usuario FROM pacientes a WHERE a.id = idbloqueado)
                END) AS bloqueado,
                razon, fechahora
            FROM bloqueos;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $idbloqueante='', $idbloqueado='',  $tipobloqueante=-1, $tipobloqueado=-1, $razon='', $fechahora=''){
        $query="INSERT INTO " . self::TABLE . " (
                idbloqueante, idbloqueado, tipobloqueante, tipobloqueado, razon, fechahora, fecultmodif) 
                VALUES (
                '$idbloqueante', '$idbloqueado', $tipobloqueante, $tipobloqueado, '$razon', '$fechahora', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
             $idbloqueante='', $idbloqueado='',  $tipobloqueante=-1, $tipobloqueado=-1, $razon='', $fechahora='') {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     idbloqueante = '$idbloqueante', idbloqueado = '$idbloqueado', 
                     tipobloqueante = $tipobloqueante, tipobloqueado = $tipobloqueado, 
                     razon = '$razon', fechahora = '$fechahora', 
                     fecultmodif = NOW() 
                     WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}