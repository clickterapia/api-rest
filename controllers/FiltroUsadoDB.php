<?php
/**
 * Description of FiltroUsadoDB
 *
 * @author meza
 */
class FiltroUsadoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'filtrosusados';
    
    public function getById($id=0){
        $query = "SELECT sexo, edad, especialidad, horario 
                 FROM filtrosusados
                 WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT sexo, edad, especialidad, horario, 
                (sexo + edad + especialidad + horario) AS total
            FROM filtrosusados;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $filtro='', $cantidad=-1){
        $query="INSERT INTO " . self::TABLE . " (
                filtro, cantidad, fecultmodif) 
                VALUES (
                '$filtro', $cantidad, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($sexo=0, $edad=0, $especialidad=0, 
                $horario=0) {
//        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                    sexo = sexo + $sexo, edad = edad + $edad, 
                    especialidad = especialidad + $especialidad, horario = horario + $horario,
                    fecultmodif = NOW();";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
//        }
//        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}