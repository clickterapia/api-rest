<?php
/**
 * Description of ConfiguracionDB
 *
 * @author meza
 */
class ConfiguracionDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'configuraciones';
    
    public function getById($id=0){
        $query = "SELECT id, porc_market_fee
            FROM configuraciones
            WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, porc_market_fee
            FROM configuraciones";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($porc_market_fee=0){
        $query="INSERT INTO " . self::TABLE . " (
                porc_market_fee, fecultmodif) 
            VALUES (
                $porc_market_fee, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $porc_market_fee=0) {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                porc_market_fee = $porc_market_fee, fecultmodif = NOW()
                WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
