<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PremiumxProfesionalDB
 *
 * @author meza
 */
class PremiumxProfesionalDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'premiumsxprofesionales';
    
    public function getById($id=0){
        $query = "SELECT id, idprofesional, idpremium, fecinicio, fecfin, costo
                 FROM premiumsxprofesionales
                 WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT p.id, p.idprofesional, p.idpremium, p.fecinicio, p.fecfin, p.costo
                FROM premiumsxprofesionales p;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $id='', $idprofesional='', $idpremium=-1,  $fecinicio='', $fecfin='', $costo=-1){
        $query="INSERT INTO " . self::TABLE . " (
                id, idprofesional, idpremium, fecinicio, fecfin, costo, fecultmodif) 
                VALUES (
                '$id', '$idprofesional', $idpremium, '$fecinicio', '$fecfin', $costo, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id='', $idprofesional='', $idpremium=-1,  $fecinicio='', $fecfin='', $costo=-1) {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     idprofesional = '$idprofesional', idpremium = $idpremium, 
                     fecinicio = '$fecinicio', fecfin = '$fecfin', 
                     costo = $costo, fecultmodif = NOW() 
                     WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}