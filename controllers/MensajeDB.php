<?php
/**
 * Description of MensajeDB
 *
 * @author meza
 */
class MensajeDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'mensajes';
    
    public function getById($id=0){
        $query = "SELECT id, idorigen, iddestino, tipoorigen, tipodestino, mensaje, fechahora, leido
                 FROM mensajes
                 WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, idorigen, iddestino, tipoorigen,
                (CASE  
                        WHEN tipoorigen = 0 THEN 'Administracion'
                        WHEN tipoorigen = 2 THEN (SELECT CONCAT(r.apellido, ', ', r.nombre) FROM profesionales r WHERE r.id = idorigen)
                        WHEN tipoorigen = 1 THEN (SELECT a.usuario FROM pacientes a WHERE a.id = idorigen)
                END) AS origen, 
                tipodestino,
                (CASE 
                        WHEN tipodestino = 0 THEN 'Administracion'
                        WHEN tipodestino = 2 THEN (SELECT CONCAT(r.apellido, ', ', r.nombre) FROM profesionales r WHERE r.id = iddestino)
                        WHEN tipodestino = 1 THEN (SELECT a.usuario FROM pacientes a WHERE a.id = iddestino)
                END) AS destino,
                mensaje, fechahora, leido
            FROM mensajes;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdProfesionalXIdPaciente($idprofesional = '', $idpaciente = ''){
        $query = "SELECT me.* FROM 
                (SELECT m.id, m.idorigen, 
                    CONCAT(r.apellido, ', ', r.nombre) AS destino, 
                    iddestino, 
                    p.usuario AS origen,
                    r.fotourl AS avatar, m.tipoorigen, m.tipodestino,
                    m.mensaje, m.fechahora, m.leido
                FROM mensajes m
                LEFT JOIN pacientes p ON p.id = m.idorigen 
                LEFT JOIN profesionales r ON r.id = m.iddestino
                WHERE m.tipoorigen = 2 AND m.tipodestino = 1 AND m.idorigen = '$idpaciente' AND m.iddestino = '$idprofesional'
                UNION
                SELECT m.id, m.idorigen, 
                    p.usuario AS destino,
                    m.iddestino, 
                    CONCAT(r.apellido, ', ', r.nombre) AS origen, 
                    r.fotourl AS avatar, m.tipoorigen, m.tipodestino,
                    m.mensaje, m.fechahora, m.leido
                FROM mensajes m
                LEFT JOIN pacientes p ON p.id = m.iddestino
                LEFT JOIN profesionales r ON r.id = m.idorigen 
                WHERE m.tipoorigen = 1 AND m.tipodestino = 2 AND m.idorigen = '$idprofesional' AND m.iddestino = '$idpaciente') me
            ORDER BY me.fechahora DESC;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdPacienteXIdProfesional($idpaciente = '', $idprofesional = ''){
        $query = "SELECT me.* FROM 
                (SELECT m.id, m.idorigen, 
                    CONCAT(r.apellido, ', ', r.nombre) AS destino, 
                    iddestino, 
                    p.usuario AS origen,
                    r.fotourl AS avatar, m.tipoorigen, m.tipodestino,
                    m.mensaje, m.fechahora, m.leido
                FROM mensajes m
                LEFT JOIN pacientes p ON p.id = m.iddestino 
                LEFT JOIN profesionales r ON r.id = m.idorigen
                WHERE m.tipoorigen = 2 AND m.tipodestino = 1 AND m.idorigen = '$idprofesional' AND m.iddestino = '$idpaciente'
                UNION
                SELECT m.id, m.idorigen, 
                    CONCAT(r.apellido, ', ', r.nombre) AS origen, 
                    m.iddestino, 
                    p.usuario AS destino,
                    r.fotourl AS avatar, m.tipoorigen, m.tipodestino,
                    m.mensaje, m.fechahora, m.leido
                FROM mensajes m
                LEFT JOIN pacientes p ON p.id = m.idorigen 
                LEFT JOIN profesionales r ON r.id = m.iddestino
                WHERE m.tipoorigen = 1 AND m.tipodestino = 2 AND m.idorigen = '$idpaciente' AND m.iddestino = '$idprofesional') me
            ORDER BY me.fechahora DESC;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    // primera consulta origen admin
    // segunda consulta destino admin
    public function getByAdminXIdPaciente($idprofesional = ''){
        $query = "SELECT me.* FROM 
                (SELECT m.id, 
                    m.idorigen, u.usuario AS origen,
                    iddestino, CONCAT(r.apellido, ', ', r.nombre) AS destino, 
                    '' AS avatar, m.tipoorigen, m.tipodestino,
                    m.mensaje, m.fechahora, m.leido
                FROM mensajes m
                LEFT JOIN usuarios u ON u.id = m.idorigen
                LEFT JOIN profesionales r ON r.id = m.iddestino 
                WHERE m.tipoorigen = 0 AND m.tipodestino = 1 AND m.iddestino = '$idprofesional'                   
                UNION
                SELECT m.id, 
                    m.idorigen, CONCAT(r.apellido, ', ', r.nombre) AS origen, 
                    m.iddestino, u.usuario AS destino,
                    r.fotourl AS avatar, m.tipoorigen, m.tipodestino,
                    m.mensaje, m.fechahora, m.leido
                FROM mensajes m
                LEFT JOIN usuarios u ON u.id = m.iddestino
                LEFT JOIN profesionales r ON r.id = m.idorigen 
                WHERE m.tipoorigen = 1 AND m.tipodestino = 0 AND m.idorigen = '$idprofesional') me
            ORDER BY me.fechahora DESC;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    public function getByAdminXIdProfesional($idprofesional = ''){
        $query = "SELECT me.* FROM 
                (SELECT m.id, 
                    m.idorigen, u.usuario AS origen,
                    iddestino, CONCAT(r.apellido, ', ', r.nombre) AS destino, 
                    '' AS avatar, m.tipoorigen, m.tipodestino,
                    m.mensaje, m.fechahora, m.leido
                FROM mensajes m
                LEFT JOIN usuarios u ON u.id = m.idorigen
                LEFT JOIN profesionales r ON r.id = m.iddestino 
                WHERE m.tipoorigen = 0 AND m.tipodestino = 2 AND m.iddestino = '$idprofesional'                   
                UNION
                SELECT m.id, 
                    m.idorigen, CONCAT(r.apellido, ', ', r.nombre) AS origen, 
                    m.iddestino, u.usuario AS destino,
                    r.fotourl AS avatar, m.tipoorigen, m.tipodestino,
                    m.mensaje, m.fechahora, m.leido
                FROM mensajes m
                LEFT JOIN usuarios u ON u.id = m.iddestino
                LEFT JOIN profesionales r ON r.id = m.idorigen 
                WHERE m.tipoorigen = 2 AND m.tipodestino = 0 AND m.idorigen = '$idprofesional') me
            ORDER BY me.fechahora DESC;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getListAdmin(){
        $query = "SELECT m.id, m.idorigen, m.iddestino, m.tipoorigen,
                (CASE  
                    WHEN m.tipoorigen = 0 THEN 'Administracion'
                    WHEN m.tipoorigen = 2 THEN CONCAT(r.apellido, ', ', r.nombre)
                    WHEN m.tipoorigen = 1 THEN p.usuario
                END) AS origen, 
                m.tipodestino,
                (CASE 
                    WHEN m.tipodestino = 0 THEN 'Administracion'
                    WHEN m.tipodestino = 2 THEN CONCAT(r.apellido, ', ', r.nombre)
                    WHEN m.tipodestino = 1 THEN p.usuario
                END) AS destino,
                m.mensaje, m.fechahora, m.leido,
                (CASE  
                    WHEN m.tipoorigen = 0 THEN ''
                    WHEN m.tipoorigen = 2 THEN r.fotourl
                    WHEN m.tipoorigen = 1 THEN ''
                END) AS avatar
            FROM mensajes m
            LEFT JOIN usuarios u ON u.id = m.idorigen OR u.id = m.iddestino 
            LEFT JOIN profesionales r ON r.id = m.idorigen OR r.id = m.iddestino 
            LEFT JOIN pacientes p ON p.id = m.idorigen OR p.id = m.iddestino 
            WHERE m.tipoorigen = 0 OR m.tipodestino = 0
            GROUP BY m.id
            ORDER BY fechahora;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getListProfesional($idprofesional = ''){
        $query = "SELECT m.id, m.idorigen, m.iddestino, m.tipoorigen,
                (CASE  
                    WHEN m.tipoorigen = 0 THEN 'Administracion'
                    WHEN m.tipoorigen = 2 THEN CONCAT(r.apellido, ', ', r.nombre)
                    WHEN m.tipoorigen = 1 THEN p.usuario
                END) AS origen, 
                m.tipodestino,
                (CASE 
                    WHEN m.tipodestino = 0 THEN 'Administracion'
                    WHEN m.tipodestino = 2 THEN CONCAT(r.apellido, ', ', r.nombre)
                    WHEN m.tipodestino = 1 THEN p.usuario
                END) AS destino,
                m.mensaje, m.fechahora, m.leido,
                (CASE  
                    WHEN m.tipoorigen = 0 THEN ''
                    WHEN m.tipoorigen = 2 THEN r.fotourl
                    WHEN m.tipoorigen = 1 THEN ''
                END) AS avatar
            FROM mensajes m
            LEFT JOIN usuarios u ON u.id = m.idorigen OR u.id = m.iddestino 
            LEFT JOIN profesionales r ON r.id = m.idorigen OR r.id = m.iddestino 
            LEFT JOIN pacientes p ON p.id = m.idorigen OR p.id = m.iddestino 
            WHERE (m.tipoorigen = 2 AND m.idorigen = '$idprofesional') 
                OR (m.tipodestino = 2 AND m.iddestino = '$idprofesional')
            GROUP BY m.id
            ORDER BY fechahora;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getListPaciente($idpaciente = ''){
        $query = "SELECT m.id, m.idorigen, m.iddestino, m.tipoorigen,
                (CASE  
                    WHEN m.tipoorigen = 0 THEN 'Administracion'
                    WHEN m.tipoorigen = 2 THEN CONCAT(r.apellido, ', ', r.nombre)
                    WHEN m.tipoorigen = 1 THEN p.usuario
                END) AS origen, 
                m.tipodestino,
                (CASE 
                    WHEN m.tipodestino = 0 THEN 'Administracion'
                    WHEN m.tipodestino = 2 THEN CONCAT(r.apellido, ', ', r.nombre)
                    WHEN m.tipodestino = 1 THEN p.usuario
                END) AS destino,
                m.mensaje, m.fechahora, m.leido,
                (CASE  
                    WHEN m.tipoorigen = 0 THEN ''
                    WHEN m.tipoorigen = 2 THEN r.fotourl
                    WHEN m.tipoorigen = 1 THEN ''
                END) AS avatar
            FROM mensajes m
            LEFT JOIN usuarios u ON u.id = m.idorigen OR u.id = m.iddestino 
            LEFT JOIN profesionales r ON r.id = m.idorigen OR r.id = m.iddestino 
            LEFT JOIN pacientes p ON p.id = m.idorigen OR p.id = m.iddestino 
            WHERE (m.tipoorigen = 1 AND m.idorigen = '$idpaciente') 
                OR (m.tipodestino = 1 AND m.iddestino = '$idpaciente')
            GROUP BY m.id
            ORDER BY fechahora;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    // insertar CASE
    public function insert($idorigen='', $iddestino='',  $tipoorigen=-1, $tipodestino=-1, $mensaje='',  $fechahora='', $leido=-1){
        $id = $this->gen_uuid();
        $query="INSERT INTO mensajes (
                id, idorigen, iddestino, tipoorigen, tipodestino, mensaje, fechahora, leido, fecultmodif) 
                VALUES (
                '$id', '$idorigen', '$iddestino', $tipoorigen, $tipodestino, '$mensaje', '$fechahora', $leido, NOW());";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();        
        $stmt->close();
        
        return $id;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update(
            $id='', $idorigen='', $iddestino='',  
            $tipoorigen=-1, $tipodestino=-1, $mensaje='',  
            $fechahora='', $leido=-1) {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                        idorigen = '$idorigen', iddestino = '$iddestino', 
                        tipoorigen = $tipoorigen, tipodestino = $tipodestino, 
                        mensaje = '$mensaje', fechahora = '$fechahora', 
                        leido = $leido, fecultmodif = NOW() 
                     WHERE id = '$id';";
//           var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}