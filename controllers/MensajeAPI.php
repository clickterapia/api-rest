<?php
/**
 * Description of MensajeAPI
 *
 * @author meza
 */
class MensajeAPI extends EntityAPI {
    const GET_BYIDPROFESIONALXIDPACIENTE = 'byidprofesionalidpaciente';
    const GET_BYIDPACIENTEXIDPROFESIONAL = 'byidpacienteidprofesional';
    const GET_BYADMINXIDPROFESIONAL = 'byadminidprofesional';
    const GET_LISTADMIN = 'listadmin';
    const GET_LISTPROFESIONAL = 'listprofesional';
    const GET_LISTPACIENTE = 'listpaciente';
    const API_ACTION = 'mensaje';

    public function __construct() {
        $this->db = new MensajeDB();
        $this->fields = [];
        array_push($this->fields, 
                'idorigen',
                'iddestino',
                'tipoorigen',
                'tipodestino',
                'mensaje',
                'fechahora',
                'leido');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdProfesionalXIdPaciente = isset($id) ? $id === self::GET_BYIDPROFESIONALXIDPACIENTE : false;
        $isByIdPacienteXIdProfesional = isset($id) ? $id === self::GET_BYIDPACIENTEXIDPROFESIONAL : false;
        $isByAdminXIdProfesional = isset($id) ? $id === self::GET_BYADMINXIDPROFESIONAL : false;
        $isListAdmin = isset($id) ? $id === self::GET_LISTADMIN : false;
        $isListProfesional = isset($id) ? $id === self::GET_LISTPROFESIONAL : false;
        $isListPaciente = isset($id) ? $id === self::GET_LISTPACIENTE : false;
        
        if($isByIdPacienteXIdProfesional) {
            $idpaciente = filter_input(INPUT_GET, 'fld1');
            $idprofesional = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getByIdPacienteXIdProfesional($idpaciente, $idprofesional);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isByIdProfesionalXIdPaciente) {
            $idprofesional = filter_input(INPUT_GET, 'fld1');
            $idpaciente = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getByIdProfesionalXIdPaciente($idprofesional, $idpaciente);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isByAdminXIdProfesional) {
            $idprofesional = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByAdminXIdProfesional($idprofesional);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }  elseif ($isListAdmin) {
            $response = $this->db->getListAdmin();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isListProfesional) {
            $idprofesional = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getListAdmin($idprofesional);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isListPaciente) {
            $idpaciente = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getListPaciente($idpaciente);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idorigen, $obj->iddestino, 
                $obj->tipoorigen, $obj->tipodestino, $obj->mensaje, 
                $obj->fechahora, $obj->leido);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idorigen, $obj->iddestino, 
                $obj->tipoorigen, $obj->tipodestino, $obj->mensaje, 
                $obj->fechahora, $obj->leido);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}