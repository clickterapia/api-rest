<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EspecialidadxProfesionalDB
 *
 * @author meza
 */
class EspecialidadxProfesionalDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'especialidadesxprofesionales';
    
    public function getById($id=0){
        $query = "SELECT id, especialidad
                 FROM especialidades
                 WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT e.id, e.idprofesional, p.nombre, p.apellido, 
                e.idespecialidad, s.especialidad
                FROM especialidadesxprofesionales e 
                LEFT JOIN profesionales p ON p.id = e.idprofesional
                LEFT JOIN especialidades s ON s.id = e.idespecialidad;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $idprofesional='', $idespecialidad=-1){
        $query="INSERT INTO " . self::TABLE . " (
                idprofesional, idespecialidad, fecultmodif) 
                VALUES (
                '$idprofesional', $idespecialidad, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $idprofesional='', $idespecialidad=-1) {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     idprofesional = '$idprofesional', idespecialidad = $idespecialidad, 
                     fecultmodif = NOW() 
                     WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}
