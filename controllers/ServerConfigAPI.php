<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class ServerConfigAPI extends EntityAPI {
    const API_ACTION = 'serverconfig';
    const GET_DATE = 'date';

    public function __construct() {
	$this->db = new ServerConfigDB();
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isDate = isset($id) ? $id === self::GET_DATE : false;
        
        if ($isDate) {
          $datetime = new DateTime();
          $datetimedb = $this->db->getDBDatetime();
          var_dump($datetimedb);
          $this->response(200,"success", $datetime);
        } 
    }
    
    function processPost() {
    }
    
    function processPut() {
    }
}