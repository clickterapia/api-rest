<?php
/**
 * Description of EncuestaAPI
 *
 * @author meza
 */
class EncuestaAPI extends EntityAPI {
    const GET_BYIDPROFESIONAL = 'byidprofesional';
    const GET_BYIDPACIENTE = 'byidpaciente';
    const API_ACTION = 'encuesta';

    public function __construct() {
        $this->db = new EncuestaDB();
        $this->fields = [];
        array_push($this->fields, 
            'idencuestatipo',
            'encuesta',
            'apacientes',
            'aprofesionales',
            'booleanoverdadero',
            'booleanofalso',
            'fecinicio',
            'fecfin');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdProfesional = isset($id) ? $id === self::GET_BYIDPROFESIONAL : false;
        $isByIdPaciente = isset($id) ? $id === self::GET_BYIDPACIENTE : false;
        
        if($isByIdProfesional) {
            $id = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdProfesional($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isByIdPaciente) {
            $id = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdPaciente($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idencuestatipo, $obj->encuesta, $obj->apacientes, 
                $obj->aprofesionales, $obj->booleanoverdadero, $obj->booleanofalso, 
                $obj->fecinicio, $obj->fecfin);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idencuestatipo, $obj->encuesta, $obj->apacientes, 
                $obj->aprofesionales, $obj->booleanoverdadero, $obj->booleanofalso, 
                $obj->fecinicio, $obj->fecfin);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}