<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FraseProfesionalDB
 *
 * @author meza
 */
class FraseProfesionalDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'frasesaprofesionales';
    
    public function getById($id=0){
        $query = "SELECT id, frase, tendencia, fecinicio, fecfin 
                 FROM frasesaprofesionales
                 WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, frase, tendencia, fecinicio, fecfin 
                FROM frasesaprofesionales;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $frase='', $tendencia=-1,  $fecinicio='', $fecfin=''){
        $query="INSERT INTO " . self::TABLE . " (
                frase, tendencia, fecinicio, fecfin, fecultmodif) 
                VALUES (
                '$frase', $tendencia, '$fecinicio', '$fecfin', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $frase='', $tendencia=-1, 
            $fecinicio='', $fecfin='') {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     frase = '$frase', tendencia = $tendencia, 
                     fecinicio = '$fecinicio', fecfin = '$fecfin', 
                     fecultmodif = NOW() 
                     WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}