<?php
/**
 * Description of ProfesionalDB
 *
 * @author meza
 */
class ProfesionalDB extends EntityDB {
    protected $mysqli;
    protected $lasterror;
    protected $rootPath;
    const TABLE = 'profesionales';
    
    public function getLastError() {
        return $this->lasterror ."";
    }
    
    public function setLastError($nroerror) {
        $this->lasterror = $nroerror;
    }
    
    public function getPendientes(){
        $query = "SELECT p.id, p.idsexo, s.sexo, p.idpais, a.pais,
                p.idprovincia, r.provincia, p.usuario, p.email,
                TIMESTAMPDIFF(YEAR, p.fecnacimiento, CURDATE()) AS edad,
                p.contrasena, p.apellido, p.nombre, p.dni, 
                p.fecnacimiento, p.notificacion, p.titulo, 
                p.universidad, p.microbiografia, p.credenciales,
                p.precio, p.fotourl, p.dniurl, 
                p.titulourl, p.matriculaurl, p.fecaprobado, 
                p.fecmodificado, p.fecregistro, p.bloqueoadministrativo,
                p.mp_authorization_code, 
                (SELECT GROUP_CONCAT(x.idespecialidad SEPARATOR '; ') 
                FROM especialidadesxprofesionales x 
                WHERE x.idprofesional = p.id) AS especialidades,
                (CASE WHEN p.fecaprobado = CONVERT(0, DATETIME) 
                THEN 0 ELSE 1 END) AS aprobado, 
                (CASE WHEN LEAST(p.titulo, p.microbiografia, p.credenciales, 
                    CONCAT(p.precio, ''), p.dniurl, p.titulourl, p.matriculaurl, p.fotourl) != '' 
                THEN 0 ELSE 1 END) AS incompleto
            FROM profesionales p
            LEFT JOIN sexos s ON s.id = p.idsexo
            LEFT JOIN paises a ON a.id = p.idpais
            LEFT JOIN provincias r ON r.id = p.idprovincia
            WHERE p.fecaprobado = CONVERT(0,DATETIME);";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }

    public function getById($id=0){
        $query = "SELECT p.id, p.idsexo, p.idpais, a.pais,
		p.idprovincia, r.provincia, p.usuario, p.email, 
		p.contrasena, p.apellido, p.nombre, p.dni, 
		p.fecnacimiento, p.notificacion, p.titulo, 
		p.universidad, p.microbiografia, p.credenciales,
		p.precio, p.fotourl, p.dniurl, 
		p.titulourl, p.matriculaurl, p.fecaprobado, 
		p.fecmodificado, p.fecregistro, p.bloqueoadministrativo, 
                p.visualizaciones, p.mp_authorization_code, p.sesionesgratis,
		COUNT(DISTINCT(s.idpaciente)) AS pacientes,
		IFNULL((SELECT CONCAT(\"[{\", 
                        GROUP_CONCAT(
                            CONCAT_WS(',', 
                                CONCAT('\"id\":', x.idespecialidad), 
                                CONCAT('\"especialidad\":', '\"', e.especialidad, '\"')) 
                            SEPARATOR '},{'), \"}]\") AS especialidades
                    FROM especialidadesxprofesionales x 
                    LEFT JOIN especialidades e ON e.id = x.idespecialidad
                    WHERE x.idprofesional = '$id'), '') AS especialidades,
                (SELECT COUNT(DISTINCT n.idpaciente) 
                FROM sesiones n
                LEFT JOIN disponibilidades d ON d.id = n.iddisponibilidad
                WHERE d.idprofesional = p.id) AS atendidos
            FROM profesionales p
            LEFT JOIN provincias r ON r.id = p.idprovincia
            LEFT JOIN paises a ON a.id = p.idpais
            LEFT JOIN disponibilidades d ON d.idprofesional
            LEFT JOIN sesiones s ON s.iddisponibilidad = d.id
            WHERE p.id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare(str_replace("\\", "", $query));
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    public function getByIdSocial($idsocial=0){
        $query = "SELECT p.id, p.idsexo, p.idpais, a.pais,
		p.idprovincia, r.provincia, p.usuario, p.email, 
		p.contrasena, p.apellido, p.nombre, p.dni, 
		p.fecnacimiento, p.notificacion, p.titulo, 
		p.universidad, p.microbiografia, p.credenciales,
		p.precio, p.fotourl, p.dniurl, 
		p.titulourl, p.matriculaurl, p.fecaprobado, 
		p.fecmodificado, p.fecregistro, p.bloqueoadministrativo, 
                p.visualizaciones, p.mp_authorization_code, p.sesionesgratis,
                p.idsocial
            FROM profesionales p
            LEFT JOIN provincias r ON r.id = p.idprovincia
            LEFT JOIN paises a ON a.id = p.idpais
            LEFT JOIN disponibilidades d ON d.idprofesional
            LEFT JOIN sesiones s ON s.iddisponibilidad = d.id
            WHERE p.idsocial = '$idsocial';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare(str_replace("\\", "", $query));
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    public function getOpinionsById($id=0){
        $query = "SELECT p.id, p.usuario, es.observaciones, s.fecfin 
            FROM encuestas e
            LEFT JOIN encuestastipos t ON t.id = e.idencuestatipo
            LEFT JOIN encuestasxsesiones es ON es.idencuesta = e.id
            LEFT JOIN sesiones s ON s.id = es.idsesion
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN pacientes p ON p.id = s.idpaciente 
            WHERE e.aprofesionales = 1 
                AND t.calificacion 
                AND t.observaciones 
                AND d.idprofesional = '$id';";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare(str_replace("\\", "", $query));
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT p.id, p.idsexo, s.sexo, p.idpais, a.pais,
                p.idprovincia, r.provincia, p.usuario, p.email,
                TIMESTAMPDIFF(YEAR, p.fecnacimiento, CURDATE()) AS edad,
                p.contrasena, p.apellido, p.nombre, p.dni, 
                p.fecnacimiento, p.notificacion, p.titulo, 
                p.universidad, p.microbiografia, p.credenciales,
                p.precio, p.fotourl, p.dniurl, 
                p.titulourl, p.matriculaurl, p.fecaprobado, 
                p.fecmodificado, p.fecregistro, p.bloqueoadministrativo,
                p.mp_authorization_code, p.sesionesgratis,
                (SELECT GROUP_CONCAT(x.idespecialidad SEPARATOR '; ') 
                FROM especialidadesxprofesionales x 
                WHERE x.idprofesional = p.id) AS especialidades,
                (CASE WHEN p.fecaprobado = CONVERT(0, DATETIME) 
                THEN 0 ELSE 1 END) AS aprobado, 
                (CASE WHEN LEAST(p.titulo, p.microbiografia, p.credenciales, 
                    CONCAT(p.precio, ''), p.dniurl, p.titulourl, p.matriculaurl, p.fotourl) != '' 
                THEN 0 ELSE 1 END) AS incompleto
            FROM profesionales p
            LEFT JOIN sexos s ON s.id = p.idsexo
            LEFT JOIN paises a ON a.id = p.idpais
            LEFT JOIN provincias r ON r.id = p.idprovincia;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getHabilitados(){
        $query = "SELECT p.id, p.idsexo, s.sexo, p.idpais, a.pais, p.idprovincia, 
                r.provincia, p.usuario, p.email, 
                TIMESTAMPDIFF(YEAR, p.fecnacimiento, CURDATE()) AS edad,
                p.contrasena, p.apellido, p.nombre, p.dni, p.fecnacimiento, 
                p.notificacion, p.titulo, p.universidad, p.microbiografia, 
                p.credenciales, p.precio, p.fotourl, p.dniurl, p.titulourl, 
                p.matriculaurl, p.fecaprobado, p.fecmodificado, p.fecregistro, 
                p.bloqueoadministrativo,
                (SELECT GROUP_CONCAT(x.idespecialidad SEPARATOR '; ') 
                FROM especialidadesxprofesionales x 
                WHERE x.idprofesional = p.id) AS especialidades,
                (CASE WHEN p.fecaprobado = CONVERT(0, DATETIME) 
                THEN 0 ELSE 1 END) AS aprobado, 
                (CASE WHEN LEAST(p.titulo, p.microbiografia, p.credenciales, 
                    CONCAT(p.precio, ''), p.dniurl, p.titulourl, p.matriculaurl, 
                    p.fotourl) != '' 
                THEN 0 ELSE 1 END) AS incompleto,
                (SELECT COUNT(DISTINCT n.idpaciente) 
                FROM sesiones n
                LEFT JOIN disponibilidades d ON d.id = n.iddisponibilidad
                WHERE d.idprofesional = p.id) AS atendidos
            FROM profesionales p
            LEFT JOIN sexos s ON s.id = p.idsexo
            LEFT JOIN paises a ON a.id = p.idpais
            LEFT JOIN provincias r ON r.id = p.idprovincia
            WHERE p.fecaprobado > CONVERT(0,DATETIME) 
                AND (SELECT COUNT(id) FROM especialidadesxprofesionales WHERE idprofesional = p.id) > 0;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getReporteProfesionales() {
        $query = "SELECT t.total, c.consesiones, i.sinsesiones, a.noaprobados
            FROM
            (
                (SELECT COUNT(p.id) AS total
                FROM profesionales p) t,
                (SELECT COUNT(DISTINCT d.idprofesional) AS consesiones
                FROM sesiones s 
                LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
                WHERE s.fecinicio > NOW()) c,
                (SELECT COUNT(DISTINCT d.idprofesional) AS sinsesiones
                FROM sesiones s 
                LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
                WHERE s.fecinicio > NOW()) n,
                (SELECT 
                    (SELECT COUNT(DISTINCT d.idprofesional)
                    FROM disponibilidades d
                    WHERE 
                        d.idprofesional NOT IN 
                            (SELECT idprofesional 
                            FROM sesiones 
                            WHERE fecinicio > NOW()))
                    + 
                    (SELECT COUNT(DISTINCT id)
                    FROM profesionales
                    WHERE id NOT IN 
                        (SELECT idprofesional 
                        FROM disponibilidades)) AS sinsesiones
                FROM DUAL) i,
                (SELECT COUNT(id) noaprobados
                FROM profesionales 
                WHERE fecaprobado =CONVERT(0,DATETIME)) a);";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getReportePremiums($desde='', $hasta='') {
        $query = "SELECT COUNT(id) AS total,  0 AS consesiones, 
                0 AS sinsesiones, 0 AS noaprobados
            FROM premiumsxprofesionales
            WHERE fecinicio BETWEEN 
                STR_TO_DATE('$desde', '%Y%m%d') AND 
                STR_TO_DATE('$hasta', '%Y%m%d');";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getReporteBasicos($desde='', $hasta='') {
        $query = "SELECT (c1.total + c2.total) AS total,  0 AS consesiones, 
                0 AS sinsesiones, 0 AS noaprobados
            FROM (
                (SELECT COUNT(p.id) AS total
                FROM profesionales p
                WHERE p.id NOT IN (SELECT idprofesional FROM premiumsxprofesionales)) c1,
                (SELECT COUNT(DISTINCT p.id) AS total
                FROM profesionales p
                LEFT JOIN premiumsxprofesionales x ON x.idprofesional = p.id
                WHERE (x.fecfin < CAST('$desde' AS DATE) OR 
                    x.fecinicio > CAST('$hasta' AS DATE))) c2);";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getRendimientoGeneral($desde='', $hasta=''){
        $query = "SELECT COUNT(id) AS cantidad, precio 
            FROM sesiones 
            WHERE fecinicio BETWEEN 
                STR_TO_DATE('$desde', '%Y%m%d') AND 
                STR_TO_DATE('$hasta', '%Y%m%d')
            GROUP BY precio
            ORDER BY precio;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getRendimientoProfesional($idprofesional='', $desde='', $hasta=''){
        $query = "SELECT COUNT(s.id) AS cantidad, s.precio 
            FROM sesiones s
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            WHERE d.idprofesional = '$idprofesional' AND
                (s.fecinicio BETWEEN 
                STR_TO_DATE('$desde', '%Y%m%d') AND 
                STR_TO_DATE('$hasta', '%Y%m%d'))
            GROUP BY s.precio
            ORDER BY s.precio;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getReporteHorasOfrecidas($idprofesional='', $desde='', $hasta=''){
        $query = "SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(p.pagas) + TIME_TO_SEC(g.gratuitas))) AS total, p.pagas, g.gratuitas
            FROM
                (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(fin, inicio)))) AS pagas
                FROM disponibilidades
                WHERE idprofesional = '$idprofesional' 
                    AND gratuito = 0
                    AND inicio BETWEEN 
                        STR_TO_DATE('$desde', '%Y%m%d') AND 
                        STR_TO_DATE('$hasta', '%Y%m%d')) p,
                (SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(fin, inicio)))) AS gratuitas
                FROM disponibilidades
                WHERE idprofesional = '$idprofesional' 
                    AND gratuito = 1
                    AND inicio BETWEEN 
                        STR_TO_DATE('$desde', '%Y%m%d') AND 
                        STR_TO_DATE('$hasta', '%Y%m%d')) g;";
//            var_dump($query);
//            return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getResumenes($idprofesional='') {
        $query = "SELECT a.ausencias, b.incompleto, 0 AS plazo
            FROM
            (SELECT COUNT(p.id) AS ausencias
            FROM problemas p
            LEFT JOIN problemastipos t ON t.id = p.idproblematipo
            LEFT JOIN sesiones s ON s.id = p.idsesion
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            WHERE d.idprofesional = '$idprofesional' AND t.ausencia = 1) a,
            (SELECT COUNT(p.id) AS incompleto
            FROM profesionales p
            WHERE p.id = '$idprofesional' 
                    AND (COALESCE(p.titulo, '') = '' OR COALESCE(p.universidad, '') = '' OR COALESCE(p.microbiografia, '') = ''
                OR COALESCE(p.credenciales, '') = '' OR COALESCE(p.precio, 0) = 0 OR COALESCE(p.fotourl, '') = ''
                    OR COALESCE(p.dniurl, '') = '' OR COALESCE(p.titulourl, '') = '' OR COALESCE(p.matriculaurl, '') = '')) b;";
//            var_dump($query);
//            return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($id='', 
            $idsexo=-1, $idpais=-1, $idprovincia=-1, 
            $usuario='', $email='', $contrasena='', 
            $apellido='', $nombre='', $dni=-1, 
            $fecnacimiento='', $notificacion=-1, $titulo='',
            $universidad='', $microbiografia='', $credenciales='',
            $precio=-1, $fotourl='', $dniurl='', 
            $titulourl='', $matriculaurl='', $fecaprobado='', 
            $fecmodificado='', $fecregistro='', $bloqueoadministrativo=-1,
            $sesionesgratis=0, $idsocial=''){
        $id = ($id !== '') ? $id : $this->gen_uuid();
        $query="INSERT INTO " . self::TABLE . " (
                id, 
                idsexo, idpais, idprovincia, 
                usuario, email, contrasena, 
                apellido, nombre, dni, 
                fecnacimiento, notificacion, titulo, 
                universidad, microbiografia, credenciales,
                precio, fotourl, dniurl, 
                titulourl, matriculaurl, fecaprobado, 
                fecmodificado, fecregistro, bloqueoadministrativo, 
                sesionesgratis, idsocial, fecultmodif) 
                VALUES (
                '$id', 
                $idsexo, $idpais, $idprovincia, 
                '$usuario', '$email', '$contrasena',
                '$apellido', '$nombre', $dni, 
                '$fecnacimiento', $notificacion, '$titulo', 
                '$universidad', '$microbiografia', '$credenciales', 
                $precio, '$fotourl', '$dniurl', 
                '$titulourl', '$matriculaurl', '$fecaprobado', 
                '$fecmodificado', '$fecregistro', $bloqueoadministrativo,
                $sesionesgratis, '$idsocial', NOW());";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $lastid = $id;
        if($r == FALSE) {
            $this->setLastError($this->mysqli->errno);
            $lastid = false;
        }
        $stmt->close();
        
        return $lastid;
    }
    
    public function uploadFileFoto($id='', $filePath='') {
        $fotourl = $this->rootPath . $filePath;
        $query="UPDATE " . self::TABLE . " SET
                fotourl='$fotourl',  
                fecultmodif=NOW()
                WHERE id='$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function uploadFileDni($id='', $filePath='') {
        $dniurl = $this->rootPath . $filePath;
        $query="UPDATE " . self::TABLE . " SET
                dniurl='$dniurl',  
                fecultmodif=NOW()
                WHERE id='$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function uploadFileTitulo($id='', $filePath='') {
        $titulourl = $this->rootPath . $filePath;
        $query="UPDATE " . self::TABLE . " SET
                titulourl='$titulourl',  
                fecultmodif=NOW()
                WHERE id='$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function uploadFileMatricula($id='', $filePath='') {
        $matriculaurl = $this->rootPath . $filePath;
        $query="UPDATE " . self::TABLE . " SET
                matriculaurl='$matriculaurl',  
                fecultmodif=NOW()
                WHERE id='$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function updateBloqueo($id='', $bloqueo=0) {
        $query="UPDATE " . self::TABLE . " SET
                bloqueoadministrativo=$bloqueo,  
                fecultmodif=NOW()
                WHERE id='$id';";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function updateAprobacion($id) {
        $query="UPDATE " . self::TABLE . " SET
                fecaprobado=NOW(),  
                fecultmodif=NOW()
                WHERE id='$id';";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update( $id='', 
            $idsexo=-1, $idpais=-1, $idprovincia=-1,
            $usuario='', $email='', $contrasena='', 
            $apellido='', $nombre='', $dni=-1, 
            $fecnacimiento='', $notificacion=-1, $titulo='',
            $universidad='', $microbiografia='', $credenciales='', 
            $precio=-1, $fotourl='', $dniurl='', 
            $titulourl='',$matriculaurl='', $fecaprobado='', 
            $fecmodificado='',$fecregistro='', $bloqueoadministrativo=-1,
            $sesionesgratis=0, $especialidades='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     idsexo = $idsexo, idpais = $idpais, 
                     idprovincia = $idprovincia, usuario = '$usuario', 
                     email = '$email', contrasena = '$contrasena', 
                     apellido = '$apellido', nombre = '$nombre', dni = $dni, 
                     fecnacimiento = '$fecnacimiento', notificacion = $notificacion, 
                     titulo = '$titulo', universidad = '$universidad', 
                     microbiografia = '$microbiografia', credenciales = '$credenciales', precio = $precio, 
                     dniurl = '$dniurl', 
                     titulourl = '$titulourl', matriculaurl = '$matriculaurl', 
                     fecaprobado = '$fecaprobado', 
                     fecmodificado = '$fecmodificado', fecregistro = '$fecregistro', 
                     bloqueoadministrativo = $bloqueoadministrativo, sesionesgratis=$sesionesgratis,
                     fecultmodif = NOW() 
                     WHERE id = '$id';";
//            var_dump($query);
//            return true;
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            $this->updateEspecialidades($id, $especialidades);
            return $r;
        }
        return false;
    }
    
    protected function updateEspecialidades($id='', $especialidades='') {
        $query = "DELETE FROM especialidadesxprofesionales WHERE idprofesional = '$id'";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        
        $query = "INSERT INTO especialidadesxprofesionales 
                (idprofesional, idespecialidad)
            SELECT '$id', id FROM especialidades WHERE id IN ($especialidades)";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        return true;
    }

    public function login($usuario='', $contrasena=''){
        $query = "SELECT id FROM profesionales WHERE usuario='$usuario' AND contrasena='$contrasena'";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $r = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return (count($r)>0) ? $r : -1;
    }

    public function loginSocial($id, $idsocial='', $fotourl=''){
//        $query = "SELECT id FROM profesionales WHERE id='$sub'";
        $query = "UPDATE " . self::TABLE 
                . " SET idsocial = '" . $idsocial . "', fotourl = '" . $fotourl
                . "' WHERE id = '" . $id . "'";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
//        $stmt = $this->mysqli->prepare($query);
//        $stmt->execute();
//        $result = $stmt->get_result();
//        $r = $result->fetch_all(MYSQLI_ASSOC);
//        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}