<?php
/**
 * Description of EncuestaxSesionAPI
 *
 * @author meza
 */
class EncuestaXSesionAPI extends EntityAPI {
    const PUT_PUBLICAR = 'publicar';
    const API_ACTION = 'encuestaxsesion';

    public function __construct() {
        $this->db = new EncuestaXSesionDB();
        $this->fields = [];
        array_push($this->fields, 
                'idsesion',
                'idencuesta',
                'calificacion',
                'valorbooleano',
                'observaciones');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idsesion, $obj->idencuesta, $obj->calificacion, 
                $obj->valorbooleano, $obj->observaciones);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        $id = filter_input(INPUT_GET, 'id');
        $isPublicar = isset($id) ? $id === self::PUT_PUBLICAR : false;
        
        if ($isPublicar) {
            $id = filter_input(INPUT_GET, 'fld1');
            $r = $this->db->publicar($id);
            if($r) { $this->response(200,"success","Record updated"); }
            else { $this->response(204,"success","Record not updated");}
            exit;
        }
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idsesion, $obj->idencuesta, $obj->calificacion, 
                $obj->valorbooleano, $obj->observaciones);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}