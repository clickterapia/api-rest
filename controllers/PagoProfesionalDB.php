<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PagoProfesionalDB
 *
 * @author meza
 */
class PagoProfesionalDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'pagosprofesionales';
    
    public function getById($id=0){
        $query = "SELECT id, fechapago, idprofesional, plazo, monto
                 FROM pagosprofesionales
                 WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT p.id, p.fechapago, p.idprofesional, 
                    r.nombre, r.apellido, p.plazo, p.monto
                FROM pagosprofesionales p
                LEFT JOIN profesionales r ON r.id = p.idprofesional;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $id='', $fechapago='', $idprofesional='',  $plazo=-1, $monto=-1){
        $query="INSERT INTO " . self::TABLE . " (
                id, fechapago, idprofesional, plazo, monto, fecultmodif) 
                VALUES (
                '$id', '$fechapago', '$idprofesional', $plazo, $monto, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id='', $fechapago='', $idprofesional='',  $plazo=-1, $monto=-1) {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     fechapago = '$fechapago', idprofesional = '$idprofesional', 
                     plazo = $plazo, monto = $monto, fecultmodif = NOW() 
                     WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}