<?php
/**
 * Description of BannerDB
 *
 * @author meza
 */
class EncuestaTipoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'encuestastipos';
    
    public function getById($id=0){
        $query = "SELECT id, encuestatipo, calificacion, observaciones, booleano 
            FROM encuestastipos
            WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, encuestatipo, calificacion, observaciones, booleano 
            FROM encuestastipos";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($encuestatipo='', $calificacion,  $observaciones, 
            $booleano){
        $query="INSERT INTO " . self::TABLE . " (
                encuestatipo, calificacion, observaciones, 
                booleano) 
            VALUES (
                '$encuestatipo', $calificacion,  $observaciones, 
                $booleano);";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $encuestatipo='', $calificacion=0,  $observaciones=0, 
            $booleano=0) {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                encuestatipo = '$encuestatipo', calificacion = $calificacion, 
                observaciones = $observaciones, booleano = booleano
                WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
