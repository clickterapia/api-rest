<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RolDB
 *
 * @author WebDev
 */
class RolDB extends EntityDB {
    protected $mysqli;
    const TABLE = 'roles';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM roles WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query('SELECT * FROM roles');
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($rol=''){
        $query="INSERT INTO roles (rol) VALUES ('$rol');";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    
    public function update($id, $rol) {
        $query = "UPDATE roles SET rol='$rol' WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }  
}
