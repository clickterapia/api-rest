<?php
/**
 * Description of FiltroUsadoAPI
 *
 * @author meza
 */
class FiltroUsadoAPI extends EntityAPI {
    const API_ACTION = 'filtrousado';

    public function __construct() {
        $this->db = new FiltroUsadoDB();
        $this->fields = [];
        array_push($this->fields,
                'sexo', 
                'edad', 
                'especialidad', 
                'horario');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->filtro, $obj->cantidad);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
//        $id = filter_input(INPUT_GET, 'id');
//        if(!$id) {
//            $this->response(422,"error","Id no enviado.");
//            exit;
//        }
        $r = $this->db->update(
                $obj->sexo, $obj->edad, $obj->especialidad, 
                $obj->horario);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}