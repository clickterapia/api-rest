<?php
/**
 * Description of DisponibilidadAPI
 *
 * @author meza
 */
class DisponibilidadAPI extends EntityAPI {
    const GET_NEWSBYIDPROFESIONAL = 'newsbyidprofesional';
    const GET_DISPONIBILIDADACTUAL = 'disponibilidadactual';
    const PUT_VARIOS = 'varios';
    const API_ACTION = 'disponibilidad';

    public function __construct() {
        $this->db = new DisponibilidadDB();
        $this->fields = [];
        array_push($this->fields, 
                'idprofesional',
                'inicio',
                'fin',
                'gratuito',
                'temporal');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isNewsByIdProfesional = isset($id) ? $id === self::GET_NEWSBYIDPROFESIONAL : false;
        $isDisponibilidadActual = isset($id) ? $id === self::GET_DISPONIBILIDADACTUAL : false;
        
        if($isNewsByIdProfesional){
            $id = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getNewsByIdProfesional($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isDisponibilidadActual) {
            $response = $this->db->getDisponibilidadActual();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idprofesional, $obj->inicio, 
                $obj->fin, $obj->gratuito, $obj->temporal);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $isVarios = isset($id) ? $id === self::PUT_VARIOS : false;
        if($isVarios) {
            $id = filter_input(INPUT_GET, 'fld1');
            $obj = json_decode(file_get_contents('php://input') );
            $r = $this->db->updateVarios($id, $obj);
            
//            var_dump(count($obj));
            $this->response(200,"success","Record updated"); 
//            if($r) { $this->response(200,"success","Record updated"); }
//            else { $this->response(204,"success","Record not updated");}
            exit;
        }
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idprofesional, $obj->inicio, 
                $obj->fin, $obj->gratuito, $obj->temporal);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}
