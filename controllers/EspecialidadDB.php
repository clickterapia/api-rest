<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EspecialidadDB
 *
 * @author meza
 */
class EspecialidadDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'especialidades';
    
    public function getById($id=0){
        $query = "SELECT id, especialidad
                 FROM especialidades
                 WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, especialidad
                FROM especialidades;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $especialidad=''){
        $query="INSERT INTO " . self::TABLE . " (
                especialidad, fecultmodif) 
                VALUES (
                '$especialidad', NOW());";
 //       var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $especialidad='', $fecinicio='', 
            $fecfin='', $imagenurl='') {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     especialidad = '$especialidad', 
                     fecultmodif = NOW() 
                     WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}
