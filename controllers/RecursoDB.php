<?php
/**
 * Description of RecursoDB
 *
 * @author meza
 */
class RecursoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'recursos';
    
    public function getById($id=0){
        $query = "SELECT id, titulo, recursos, url, video, 
                primeros, comerciales, tecnicos
            FROM recursos
            WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT r.id, r.titulo, r.recursos, r.url, r.video,
                primeros, comerciales, tecnicos
            FROM recursos r;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( 
            $titulo='', $recursos='', $url='', 
            $video=-1, $primeros=0, $comerciales=0,
            $tecnicos=0){
        $query="INSERT INTO " . self::TABLE . " (
                titulo, recursos, url, 
                video, primeros, comerciales,
                tecnicos, fecultmodif) 
            VALUES (
                '$titulo', '$recursos', '$url', 
                $video, $primeros, $comerciales,
                $tecnicos, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $titulo='', $recursos='', $url='', 
            $video=-1, $primeros=0, $comerciales=0,
            $tecnicos=0) {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                    titulo = '$titulo', recursos = '$recursos', url = '$url',
                    video = $video, primeros = $primeros, comerciales = $comerciales,
                    tecnicos = $tecnicos, fecultmodif = NOW() 
                WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}