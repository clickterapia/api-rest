<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoProblemaEspecificoDB
 *
 * @author meza
 */
class TipoProblemaEspecificoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'tiposproblemasespecificos';
    
    public function getById($id=0){
        $query = "SELECT id, tipoespecifico, fecultmodif
                 FROM tiposproblemasespecificos
                 WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, tipoespecifico, fecultmodif
                FROM tiposproblemasespecificos;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $tipoespecifico=''){
        $query="INSERT INTO " . self::TABLE . " (
                tipoespecifico, fecultmodif) 
                VALUES (
                '$tipoespecifico', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $tipoespecifico='') {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     tipoespecifico = '$tipoespecifico',
                     fecultmodif = NOW() 
                     WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}