<?php
/**
 * Description of ContactoWebDB
 *
 * @author meza
 */
class ContactoWebDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'contactosweb';
    
    public function getById($id=0){
        $query = "SELECT id, email, contactado, fecultmodif
            FROM contactosweb
            WHERE id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT id, email, contactado, fecultmodif
            FROM contactosweb";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($email='', $contactado=0){
        $query="INSERT INTO " . self::TABLE . " (
                email, contactado, fecultmodif) 
            VALUES (
                '$email', $contactado, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $email='', $contactado=0) {
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                email = '$email', contactado = $contactado, fecultmodif = NOW()
                WHERE id = $id;";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
