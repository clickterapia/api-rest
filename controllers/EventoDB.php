<?php
/**
 * Description of EventoDB
 *
 * @author meza
 */
class EventoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'eventos';
    
    public function getById($id=0){
        $query = "SELECT id, idsesion, idpaciente, idprofesional, idtipoevento, evento, horaevento
                 FROM eventos
                 WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getFindXdata($idpaciente, $idprofesional, $fecha) {
        $query = "SELECT e.id, e.idsesion, s.fecinicio, e.idpaciente, 
                p.usuario, e.idprofesional, o.apellido, o.nombre, 
                CONCAT(o.apellido, o.nombre) AS profesional,  e.idtipoevento, 
                v.eventotipo, e.evento, e.horaevento 
            FROM eventos e
            LEFT JOIN sesiones s ON s.id = e.idsesion
            LEFT JOIN pacientes p ON p.id = e.idpaciente
            LEFT JOIN profesionales o ON o.id = e.idprofesional
            LEFT JOIN eventostipos v ON v.id = e.idtipoevento
            WHERE e.idpaciente = '$idpaciente' 
                AND e.idprofesional = '$idprofesional'
                AND DATE(s.fecinicio) = STR_TO_DATE('$fecha', '%Y%m%d');";
//        var_dump($query);
//        return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT e.id, e.idsesion, s.fecinicio, e.idpaciente, 
                    p.usuario, e.idprofesional, o.apellido, o.nombre,
                    e.idtipoevento, e.evento, e.horaevento 
                FROM eventos e
                LEFT JOIN sesiones s ON s.id = e.idsesion
                LEFT JOIN pacientes p ON p.id = e.idpaciente
                LEFT JOIN profesionales o ON o.id = e.idprofesional
                LEFT JOIN eventostipos v ON v.id = e.idtipoevento;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $id='', $idsesion='', $idpaciente='',  $idprofesional='',  $idtipoevento=-1,  $evento='', $horaevento=''){
        $query="INSERT INTO " . self::TABLE . " (
                id, idsesion, idpaciente, idprofesional, idtipoevento, evento, horaevento, fecultmodif) 
                VALUES (
                '$id', '$idsesion', '$idpaciente', '$idprofesional', $idtipoevento, '$evento', '$horaevento', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id='', 
            $idsesion='', $idpaciente='',  
            $idprofesional='',  $idtipoevento=-1,  
            $evento='', $horaevento='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     idsesion = '$idsesion', idpaciente = '$idpaciente', 
                     idprofesional = '$idprofesional', idtipoevento = $idtipoevento, 
                     evento = '$evento', horaevento = '$horaevento', 
                     fecultmodif = NOW() 
                     WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}