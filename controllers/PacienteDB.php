<?php
/**
 * Description of PacienteDB
 *
 * @author meza
 */
class PacienteDB extends EntityDB {
   protected $mysqli;
    protected $lasterror;
   const TABLE = 'pacientes';
    
    public function getLastError() {
        return $this->lasterror ."";
    }
    
    public function setLastError($nroerror) {
        $this->lasterror = $nroerror;
    }
    
    public function getById($id=0){
        $query = "SELECT id, idsexo, idpais, idprovincia, 
                fecnacimiento, usuario, email, contrasena, 
                notificacion, historialclinico, fecregistro, 
                bloqueoadministrativo
            FROM pacientes
            WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }

    public function getByIdSocial($idsocial=0){
        $query = "SELECT id, idsexo, idpais, idprovincia, 
                fecnacimiento, usuario, email, contrasena, 
                notificacion, historialclinico, fecregistro, 
                bloqueoadministrativo, idsocial
            FROM pacientes
            WHERE idsocial = '$idsocial';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare(str_replace("\\", "", $query));
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT p.id, p.idsexo, s.sexo, 
                    p.idpais, a.pais, p.idprovincia, r.provincia,
                    p.fecnacimiento, p.usuario, p.email, p.contrasena, 
                    p.notificacion, p.historialclinico, p.fecregistro,
                    p.bloqueoadministrativo
                FROM pacientes p
                LEFT JOIN sexos s ON s.id = p.idsexo
                LEFT JOIN paises a ON a.id = p.idpais
                LEFT JOIN provincias r ON r.id = p.idprovincia;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdProfesional($id=0){
        $query = "SELECT s.idpaciente, p.usuario, p.idsexo, x.sexo, 
                    p.idpais, a.pais, p.idprovincia, r.provincia,
                    p.fecnacimiento, p.usuario, p.email, p.contrasena, 
                    p.notificacion, p.historialclinico, p.fecregistro,
                    p.bloqueoadministrativo,
                    (SELECT MAX(s1.fecinicio) 
                    FROM sesiones s1
                    LEFT JOIN disponibilidades d1 ON d1.id = s1.iddisponibilidad
                    WHERE s1.idpaciente = s.idpaciente AND d1.idprofesional = d.idprofesional) AS primera,
                    (SELECT MIN(s1.fecinicio) 
                    FROM sesiones s1 
                    LEFT JOIN disponibilidades d1 ON d1.id = s1.iddisponibilidad
                    WHERE s1.idpaciente = s.idpaciente AND d1.idprofesional = d.idprofesional) AS ultima,
                    (SELECT COUNT(s1.fecinicio) 
                    FROM sesiones s1
                    LEFT JOIN disponibilidades d1 ON d1.id = s1.iddisponibilidad
                    WHERE s1.idpaciente = s.idpaciente AND d1.idprofesional = d.idprofesional) AS cantidad
            FROM sesiones s
            LEFT JOIN pacientes p ON p.id = s.idpaciente
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN sexos x ON x.id = p.idsexo
            LEFT JOIN paises a ON a.id = p.idpais
            LEFT JOIN provincias r ON r.id = p.idprovincia
            WHERE d.idprofesional = '$id' 
            GROUP BY s.idpaciente;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getByIdeIdProfesional($id='', $idprofesional=''){
        $query = "SELECT s.idpaciente AS id, p.usuario, p.idsexo, x.sexo, 
                    p.idpais, a.pais, p.idprovincia, r.provincia,
                    p.fecnacimiento, p.usuario, p.email, p.contrasena, 
                    p.notificacion, p.historialclinico, p.fecregistro,
                    p.bloqueoadministrativo,
                    (SELECT MAX(s1.fecinicio) 
                    FROM sesiones s1
                    LEFT JOIN disponibilidades d1 ON d1.id = s1.iddisponibilidad
                    WHERE s1.idpaciente = '$id' AND d1.idprofesional = '$idprofesional') AS primera,
                    (SELECT MIN(s1.fecinicio) 
                    FROM sesiones s1 
                    LEFT JOIN disponibilidades d1 ON d1.id = s1.iddisponibilidad
                    WHERE s1.idpaciente = '$id' AND d1.idprofesional ='$idprofesional') AS ultima,
                    (SELECT COUNT(s1.fecinicio) 
                    FROM sesiones s1
                    LEFT JOIN disponibilidades d1 ON d1.id = s1.iddisponibilidad
                    WHERE s1.idpaciente = '$id' AND d1.idprofesional = '$idprofesional') AS cantidad
            FROM sesiones s
            LEFT JOIN pacientes p ON p.id = s.idpaciente
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN sexos x ON x.id = p.idsexo
            LEFT JOIN paises a ON a.id = p.idpais
            LEFT JOIN provincias r ON r.id = p.idprovincia
            WHERE d.idprofesional = '$idprofesional';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getConSesiones() {
        $query = "SELECT p.id, p.idsexo, s.sexo, 
                p.idpais, a.pais, p.idprovincia, r.provincia,
                p.fecnacimiento, 
                TIMESTAMPDIFF(YEAR, p.fecnacimiento, CURDATE()) AS edad,
                e.fecinicio, p.usuario, p.email, p.contrasena, 
                p.notificacion, p.historialclinico, p.fecregistro,
                p.bloqueoadministrativo,
                COUNT(e.id) AS cantidad,
                MAX(e.fecinicio) AS ultima,
                MIN(e.fecinicio) AS primera 
            FROM sesiones e
            LEFT JOIN pacientes p ON p.id = e.idpaciente
            LEFT JOIN sexos s ON s.id = p.idsexo
            LEFT JOIN paises a ON a.id = p.idpais
            LEFT JOIN provincias r ON r.id = p.idprovincia
            GROUP BY p.id
            ORDER BY e.fecinicio ASC;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getResumenes($idpaciente='') {
        $query = "SELECT a.ausencias, 0 AS incompleto, 0 AS plazo
            FROM
            (SELECT COUNT(p.id) AS ausencias
            FROM problemas p
            LEFT JOIN problemastipos t ON t.id = p.idproblematipo
            LEFT JOIN sesiones s ON s.id = p.idsesion
            WHERE s.idpaciente = '$idpaciente' AND t.ausencia = 1) a;";
//            var_dump($query);
//            return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($id='', 
            $idsexo=-1, $idpais=-1, $idprovincia=-1, 
            $fecnacimiento='', $usuario='', $email='', 
            $contrasena='', $notificacion=-1, $historialclinico='', 
            $fecregistro='', $bloqueoadministrativo=-1){
        $id = ($id !== '') ? $id : $this->gen_uuid();
        $query="INSERT INTO pacientes (id, 
                idsexo, idpais, idprovincia, 
                fecnacimiento, usuario, email, 
                contrasena, notificacion, historialclinico, 
                fecregistro, bloqueoadministrativo, fecultmodif) 
            VALUES ('$id',
                $idsexo, $idpais, $idprovincia,
                '$fecnacimiento', '$usuario', '$email',
                '$contrasena', $notificacion, '$historialclinico',
                '$fecregistro', $bloqueoadministrativo, NOW());";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);$r = $stmt->execute();
        $lastid = $id;
        if($r == FALSE) {
            $this->setLastError($this->mysqli->errno);
            $lastid = false;
        }
        $stmt->close();
        
        return $lastid;
    }
    
    public function update(
            $id='', $idsexo=-1, $idpais=-1,  
            $idprovincia=-1, $fecnacimiento='', $usuario='',  
            $email='', $contrasena='', $notificacion=-1, 
            $historialclinico='',  $fecregistro='', $bloqueoadministrativo=-1) {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                        idsexo = $idsexo, idpais = $idpais, 
                        idprovincia = $idprovincia, fecnacimiento = '$fecnacimiento', 
                        usuario = '$usuario', email = '$email', 
                        contrasena = '$contrasena', notificacion = $notificacion, 
                        historialclinico = '$historialclinico', fecregistro = '$fecregistro', 
                        bloqueoadministrativo = $bloqueoadministrativo, fecultmodif = NOW() 
                     WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function updateHistorial(
            $id='', $historialclinico='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                        historialclinico = '$historialclinico', fecultmodif = NOW() 
                     WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }

    public function login($usuario='', $contrasena=''){
        $query = "SELECT id FROM pacientes WHERE usuario='$usuario' AND contrasena='$contrasena'";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $r = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return (count($r)>0) ? $r : -1;
    }

    public function loginSocial($id='', $idsocial='', $fotourl=''){
        $query = "UPDATE " . self::TABLE 
                . " SET idsocial = '" . $idsocial . "' WHERE id = '" . $id . "'";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        return $r;
//        $query = "SELECT id FROM pacientes WHERE id='$sub'";
//        $stmt = $this->mysqli->prepare($query);
//        $stmt->execute();
//        $result = $stmt->get_result();
//        $r = $result->fetch_all(MYSQLI_ASSOC);
//        $stmt->close();
//        return (count($r)>0) ? $r : -1;
    }
    
    public function updateBloqueo($id='', $bloqueo=0) {
        $query="UPDATE " . self::TABLE . " SET
                bloqueoadministrativo=$bloqueo,  
                fecultmodif=NOW()
                WHERE id='$id';";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}