<?php
/**
 * Description of DisponibilidadDB
 *
 * @author meza
 */
class DisponibilidadDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'disponibilidades';
    
    public function getById($id=0){
        $query = "SELECT id, idprofesional, inicio, fin, gratuito, temporal
                 FROM disponibilidades
                 WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT d.id, d.idprofesional, p.apellido, p.nombre, 
                d.inicio, d.fin, d.gratuito,
                d.temporal, 
                (CASE IFNULL(s.id, 0) WHEN 0 THEN 0 ELSE 1 END) AS reservado
            FROM disponibilidades d 
            LEFT JOIN profesionales p ON p.id = d.idprofesional
            LEFT JOIN sesiones s ON s.iddisponibilidad = d.id";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getNewsByIdProfesional($id=''){
        $query = "SELECT d.id, d.idprofesional, p.apellido, p.nombre, 
                d.inicio, d.fin, d.gratuito,
                d.temporal, 
                (CASE IFNULL(s.id, 0) WHEN 0 THEN 0 ELSE 1 END) AS reservado
            FROM disponibilidades d 
            LEFT JOIN profesionales p ON p.id = d.idprofesional
            LEFT JOIN sesiones s ON s.iddisponibilidad = d.id
            WHERE idprofesional = '$id' AND inicio > NOW();";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getDisponibilidadActual(){
        $query = "SELECT d.id, d.idprofesional, p.apellido, p.nombre, 
                d.inicio, d.fin, d.gratuito,
                d.temporal, 
                (CASE IFNULL(s.id, 0) WHEN 0 THEN 0 ELSE 1 END) AS reservado
            FROM disponibilidades d 
            LEFT JOIN profesionales p ON p.id = d.idprofesional
            LEFT JOIN sesiones s ON s.iddisponibilidad = d.id
            WHERE d.inicio > NOW();";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $id='', $idprofesional='', $inicio='',  $fin='', $gratuito=-1, $temporal=''){
        $query="INSERT INTO " . self::TABLE . " (
                id, idprofesional, inicio, fin, gratuito, temporal, fecultmodif) 
                VALUES (
                '$id', '$idprofesional', '$inicio', '$fin', $gratuito, '$temporal', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id='', 
            $idprofesional='', $inicio='', 
            $fin='', $gratuito=-1, $temporal='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                     idprofesional = '$idprofesional', inicio = '$inicio', 
                     fin = '$fin', gratuito = $gratuito, 
                     temporal = '$temporal', fecultmodif = NOW() 
                     WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function updateVarios($idprofesional='', $events=null) {
        // elimino todos los eventos
        $query = "DELETE FROM disponibilidades 
            WHERE idprofesional = '$idprofesional' 
                AND id NOT IN (SELECT s.iddisponibilidad FROM sesiones s WHERE s.iddisponibilidad = id)
                AND inicio > '"  . date("Y-m-d") . " 00:00:00';";
        $stmt = $this->mysqli->prepare($query);

        $r = $stmt->execute(); 
        $stmt->close();

        if(count($events) === 0) {
            return true;
        }
        foreach($events as $event){
            if($event->reservado == 0) {
                $id = $this->gen_uuid();
                $r = $this->insert($id, $idprofesional, $event->inicio, $event->fin, $event->gratuito, '');
            }
        }
        return true;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}