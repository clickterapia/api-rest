<?php
/**
 * Description of EncuestaDB
 *
 * @author meza
 */
class EncuestaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'encuestas';
    
    public function getById($id=0){
        $query = "SELECT id, idencuestatipo, encuesta, apacientes, 
                aprofesionales, booleanoverdadero, booleanofalso, 
                fecinicio, fecfin, fecultmodif
            FROM encuestas
            WHERE id = '$id';";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT e.id, e.idencuestatipo, t.encuestatipo, e.encuesta, 
                e.apacientes, e.aprofesionales, e.booleanoverdadero, e.booleanofalso, 
                e.fecinicio, e.fecfin, e.fecultmodif
            FROM encuestas e
            LEFT JOIN encuestastipos t ON t.id = e.idencuestatipo;";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdProfesional($idprofesional=''){
        $query = "SELECT e.id, e.idencuestatipo, t.encuestatipo, t.calificacion, 
                t.observaciones, t.booleano, e.encuesta, e.apacientes, 
                e.aprofesionales, e.booleanoverdadero, e.booleanofalso, 
                e.fecinicio, e.fecfin, e.fecultmodif
            FROM encuestas e
            LEFT JOIN encuestastipos t ON t.id = e.idencuestatipo
            WHERE e.aprofesionales = 1 
                AND e.id NOT IN 
                    (SELECT x.idencuesta 
                    FROM encuestasxsesiones x
                    LEFT JOIN sesiones s ON s.id = x.idsesion
                    LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
                    WHERE d.idprofesional = '$idprofesional')
                AND (NOW() BETWEEN e.fecinicio AND e.fecfin)
            ORDER BY e.fecinicio ASC;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getByIdPaciente($idpaciente=''){
        $query = "SELECT e.id, e.idencuestatipo, t.encuestatipo, t.calificacion, 
                t.observaciones, t.booleano, e.encuesta, e.apacientes, 
                e.aprofesionales, e.booleanoverdadero, e.booleanofalso, 
                e.fecinicio, e.fecfin, e.fecultmodif
            FROM encuestas e
            LEFT JOIN encuestastipos t ON t.id = e.idencuestatipo
            WHERE e.apacientes = 1 
                AND e.id NOT IN 
                    (SELECT x.idencuesta 
                    FROM encuestasxsesiones x
                    LEFT JOIN sesiones s ON s.id = x.idsesion
                    WHERE s.idpaciente = '$idpaciente')
                AND (NOW() BETWEEN e.fecinicio AND e.fecfin)
            ORDER BY e.fecinicio ASC;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function insert($idencuestatipo=-1, $encuesta='', $apacientes=0, 
            $aprofesionales=0, $booleanoverdadero='', $booleanofalso='', 
            $fecinicio='', $fecfin=''){
        $id = self::gen_uuid();
        $query="INSERT INTO " . self::TABLE . " ( id,
                idencuestatipo, encuesta, apacientes, 
                aprofesionales, booleanoverdadero, booleanofalso, 
                fecinicio, fecfin, fecultmodif) 
            VALUES ('$id',
                $idencuestatipo, '$encuesta', $apacientes, 
                $aprofesionales, '$booleanoverdadero', '$booleanofalso', 
                '$fecinicio', '$fecfin', NOW());";
//        var_dump($query);
//        return true;
        
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        return $id;
    }
    
    public function update($id='', 
            $idencuestatipo=-1, $encuesta='', $apacientes=0, 
            $aprofesionales=0, $booleanoverdadero='', $booleanofalso='', 
            $fecinicio='', $fecfin='') {
        if($this->checkStringID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET 
                    idencuestatipo = $idencuestatipo, encuesta='$encuesta', apacientes=$apacientes, 
                    aprofesionales=$aprofesionales, booleanoverdadero='$booleanoverdadero', booleanofalso='$booleanofalso', 
                    fecinicio='$fecinicio', fecfin='$fecfin', fecultmodif=NOW() 
                WHERE id = '$id';";
 //          var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}