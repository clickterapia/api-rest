<?php
require_once "/home/clickterapia/public_html/admin/api/controllers/EntityDB.php";
class MPDalDB extends EntityDB {
   protected $mysqli;
   
   public function getCheckoutData($idpaciente='', $idprofesionl=''){
        $query = "SELECT s.precio, a.email,
                CONCAT('Sesi贸n con el ', p.titulo, ' ', p.apellido, ' ', p.nombre) AS profesional
            FROM sesiones s
            LEFT JOIN disponibilidades d ON d.id = s.iddisponibilidad
            LEFT JOIN profesionales p ON p.id = d.idprofesional
            LEFT JOIN pacientes a ON a.id = s.idpaciente
            WHERE s.idpaciente = '$idpaciente' AND d.idprofesional = '$idprofesionl' 
                AND s.fecinicio > NOW();";
        // var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function saveLog($log='') {
        $query="INSERT INTO logs (
                log, fecultmodif) 
                VALUES (
                '$log', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function saveMPAuthenticationCode($idprofesional='', $mp_authorization_code=''){
        $query="UPDATE profesionales SET 
                mp_authorization_code = '$mp_authorization_code'
                WHERE id = '$idprofesional'";
        // var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        return ($stmt) ? true : false;
    }
}