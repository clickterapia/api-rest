<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class CheckoutAPI {
    const API_ACTION = 'checkout';

    public function __construct() {
        $this->db = new MPDalDB();
    }
    
    function process() {
        $idpaciente = filter_input(INPUT_GET, 'id');
        $idprofesional = filter_input(INPUT_GET, 'fld1');
        $data = $this->db->getCheckoutData($idpaciente, $idprofesional);
//        var_dump($data);
        //TEST data
        MercadoPago\SDK::setClientId("2345630465897153");
        MercadoPago\SDK::setClientSecret("GeLVkokM6luanE8D9AsfJSjGP5rIgQag");
        MercadoPago\SDK::setPublicKey("APP_USR-83752273-0db1-441e-b7f7-1d2b1902b5d0");
        MercadoPago\SDK::setAccessToken("APP_USR-2345630465897153-071213-60954eaf8edcea15e0fcaf818f657936-451418610");
        // Production DATA
        // MercadoPago\SDK::setClientId("7875836160014583");
        // MercadoPago\SDK::setClientSecret("0AXxEXNracecyYp7YWxv0Lm2HXydF5PV");
        // MercadoPago\SDK::setPublicKey("TEST-a79fbd02-c25c-494d-a33c-2d6dab034930");
        // MercadoPago\SDK::setAccessToken("TEST-7875836160014583-042016-63d2b0326551daa7c4f5a7b2e8d8e16e-428215671");
        
        $preference = new MercadoPago\Preference();
//        var_dump($preference);
        $email = '';
        $array=array();
        
        foreach ($data as $datum) {
            $item = new MercadoPago\Item();
            $id = 1;
            $item->id = $id;
            $item->title = $datum['profesional'];
            $item->quantity = 1;
            $item->currency_id = "ARS";
            $item->unit_price = $datum['precio'];
            $email = $datum['email'];
            array_push($array, $item);
        }
//        var_dump($array);
        $payer = new MercadoPago\Payer();
        $payer->email = $email;
        
        $preference->items = $array;
        $preference->payer = $payer;
        $preference->back_urls = array(
            "success" => "https://clickterapia.com/admin/#/dashboard/dashboard-pac",
            "failure" => "https://clickterapia.com/admin/#/dashboard/dashboard-pac",
            "pending" => "https://clickterapia.com/admin/#/dashboard/dashboard-pac"
        );
        $preference->auto_return = "approved";
        $preference->notification_url = 'https://clickterapia.com/ipn/index.php';
        // $preference->notification_url = 'https://clickterapia.com/adm/api/mercadopago/ipn/index.php';
        
        $preference->save();
        echo "<script type='text/javascript'>window.top.location='$preference->init_point';</script>"; exit;
    }
    
    function response($code=200, $status="", $message="") {
        http_response_code($code);
        if( !empty($status) && !empty($message) ){
            $response = array("status" => $status ,"message"=>$message);  
            echo json_encode($response,JSON_PRETTY_PRINT);    
        }  
    }
}