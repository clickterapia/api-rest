<?php
require '/home/clickterapia/public_html/admin/api/vendor/autoload.php';
require_once '/home/clickterapia/public_html/admin/api/mercadopago/mpdaldb.php';
require_once '/home/clickterapia/public_html/admin/api/controllers/EntityDB.php';

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
class IPNAPI  {
    const API_ACTION = 'ipn';

    public function __construct() {
        $this->db = new MPDalDB();
    }
    
    public function process() {
        //TEST data
        MercadoPago\SDK::setClientId("2345630465897153");
        MercadoPago\SDK::setClientSecret("GeLVkokM6luanE8D9AsfJSjGP5rIgQag");
        MercadoPago\SDK::setPublicKey("APP_USR-83752273-0db1-441e-b7f7-1d2b1902b5d0");
        MercadoPago\SDK::setAccessToken("APP_USR-2345630465897153-071213-60954eaf8edcea15e0fcaf818f657936-451418610");
        // Production DATA
        // MercadoPago\SDK::setClientId("7875836160014583");
        // MercadoPago\SDK::setClientSecret("0AXxEXNracecyYp7YWxv0Lm2HXydF5PV");
        // MercadoPago\SDK::setPublicKey("TEST-a79fbd02-c25c-494d-a33c-2d6dab034930");
        // MercadoPago\SDK::setAccessToken("TEST-7875836160014583-042016-63d2b0326551daa7c4f5a7b2e8d8e16e-428215671");
        
        $merchant_order = null;
        switch($_GET["id"]) {
            case "payment":
                $merchant_order = 'payment';
                $payment = MercadoPago\Payment::find_by_id($_GET["id"]);
                // Get the payment and the corresponding merchant_order reported by the IPN.
                $merchant_order = MercadoPago\MerchantOrder::find_by_id($payment->order_id);
            case "merchant_order":
                $merchant_order = 'merchant_order';
                $merchant_order = MercadoPago\MerchantOrder::find_by_id($_GET["id"]);
        }
        $id= $_GET["id"];
        // $dump = print_r($merchant_order, true);
        //$dump = print_r($_GET["id"], true);
        $this->db->saveLog($id);
        http_response_code(200);
    }
}
$ipn = new IPNAPI();
$ipn->process();
