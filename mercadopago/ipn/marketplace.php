<?php
require '/home/clickterapia/public_html/admin/api/vendor/autoload.php';
require_once '/home/clickterapia/public_html/admin/api/mercadopago/mpdaldb.php';
require_once '/home/clickterapia/public_html/admin/api/controllers/EntityDB.php';

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
class MarketPlaceAPI {
    const API_ACTION = 'marketplace';

    public function __construct() {
        $this->db = new MPDalDB();
    }
    
    function process() {
        $code = filter_input(INPUT_GET, 'code');
        $idprofesional = filter_input(INPUT_GET, 'idprofesional');
        $r = $this->db->saveMPAuthenticationCode($idprofesional, $code);
        // var_dump($_SERVER['PHP_SELF']);
        if($r) {
             echo "<script type='text/javascript'>window.top.location='https://clickterapia.com/admin/#/perfilprofesional';</script>"; exit;
        }
        http_response_code(200);
    }

    function existCurl(){
        return function_exists('curl_version');
    }
    
    function response($code=200, $status="", $message="") {
        http_response_code($code);
        if( !empty($status) && !empty($message) ){
            $response = array("status" => $status ,"message"=>$message);  
            echo json_encode($response,JSON_PRETTY_PRINT);    
        }  
    }
}

$ipn = new MarketPlaceAPI();
$ipn->process();