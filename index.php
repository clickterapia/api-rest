<?php
    define('TIMEZONE', 'America/Argentina/Buenos_Aires');
    date_default_timezone_set(TIMEZONE);
    
    require_once "./controllers/EntityAPI.php";
    require_once "./controllers/EntityDB.php";    
    require_once "./controllers/ServerConfigAPI.php";
    require_once "./controllers/ServerConfigDB.php";
    
    require_once "./controllers/RolAPI.php";
    require_once "./controllers/RolDB.php";
    require_once "./controllers/UsuarioAPI.php";
    require_once "./controllers/UsuarioDB.php";
    require_once "./controllers/BannerAPI.php";
    require_once "./controllers/BannerDB.php";
    require_once "./controllers/BloqueoAPI.php";
    require_once "./controllers/BloqueoDB.php";
    require_once "./controllers/DisponibilidadAPI.php";
    require_once "./controllers/DisponibilidadDB.php";
    require_once "./controllers/EspecialidadAPI.php";
    require_once "./controllers/EspecialidadDB.php";
    require_once "./controllers/EspecialidadxProfesionalAPI.php";
    require_once "./controllers/EspecialidadxProfesionalDB.php";
    require_once "./controllers/EventoTipoAPI.php";
    require_once "./controllers/EventoTipoDB.php";
    require_once "./controllers/FiltroUsadoAPI.php";
    require_once "./controllers/FiltroUsadoDB.php";
    require_once "./controllers/FraseProfesionalAPI.php";
    require_once "./controllers/FraseProfesionalDB.php";
    require_once "./controllers/FuncionAPI.php";
    require_once "./controllers/FuncionDB.php";
    require_once "./controllers/FuncionXPremiumAPI.php";
    require_once "./controllers/FuncionXPremiumDB.php";
    require_once "./controllers/PaisAPI.php";
    require_once "./controllers/PaisDB.php";
    require_once "./controllers/PremiumAPI.php";
    require_once "./controllers/PremiumDB.php";
    require_once "./controllers/ProblemaEspecificoAPI.php";
    require_once "./controllers/ProblemaEspecificoDB.php";
    require_once "./controllers/ProblemaespecificoxProfesionalAPI.php";
    require_once "./controllers/ProblemaespecificoxProfesionalDB.php";
    require_once "./controllers/ProblemaTipoAPI.php";
    require_once "./controllers/ProblemaTipoDB.php";
    require_once "./controllers/ProvinciaAPI.php";
    require_once "./controllers/ProvinciaDB.php";
    require_once "./controllers/RecursoAPI.php";
    require_once "./controllers/RecursoDB.php";
    require_once "./controllers/TerminoCondicionAPI.php";
    require_once "./controllers/TerminoCondicionDB.php";
    require_once "./controllers/EncuestaAPI.php";
    require_once "./controllers/EncuestaDB.php";
    require_once "./controllers/EncuestaTipoAPI.php";
    require_once "./controllers/EncuestaTipoDB.php";
    require_once "./controllers/EncuestaXSesionAPI.php";
    require_once "./controllers/EncuestaXSesionDB.php";
    require_once "./controllers/EventoAPI.php";
    require_once "./controllers/EventoDB.php";
    require_once "./controllers/MensajeAPI.php";
    require_once "./controllers/MensajeDB.php";
    require_once "./controllers/PacienteAPI.php";
    require_once "./controllers/PacienteDB.php";
    require_once "./controllers/PagoProfesionalAPI.php";
    require_once "./controllers/PagoProfesionalDB.php";
    require_once "./controllers/PremiumxProfesionalAPI.php";
    require_once "./controllers/PremiumxProfesionalDB.php";
    require_once "./controllers/ProblemaAPI.php";
    require_once "./controllers/ProblemaDB.php";
    require_once "./controllers/ProfesionalAPI.php";
    require_once "./controllers/ProfesionalDB.php";
    require_once "./controllers/SesionAPI.php";
    require_once "./controllers/SesionDB.php";
    require_once "./controllers/SexoAPI.php";
    require_once "./controllers/SexoDB.php";
    require_once "./controllers/TipoProblemaEspecificoAPI.php";
    require_once "./controllers/TipoProblemaEspecificoDB.php";
    require_once "./controllers/ContactoWebAPI.php";
    require_once "./controllers/ContactoWebDB.php";
    require_once "./controllers/ConfiguracionAPI.php";
    require_once "./controllers/ConfiguracionDB.php";
    
    // services
    require_once "./services/UploadFileService.php";
        
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Cache-Control, Pragma');
    
    $action = $_GET['action'];
    $api = NULL;
    $service = NULL;
    $mercadopago = NULL;
    
    switch($action) {
        
        case ServerConfigAPI::API_ACTION:
            $api = new ServerConfigAPI();
            break;
        
        case RolAPI::API_ACTION:
            $api = new RolAPI();
            break;
        case UsuarioAPI::API_ACTION:
            $api = new UsuarioAPI();
            break;
        case BannerAPI::API_ACTION:
            $api = new BannerAPI();
            break;
        case BloqueoAPI::API_ACTION:
            $api = new BloqueoAPI();
            break;
        case DisponibilidadAPI::API_ACTION:
            $api = new DisponibilidadAPI();
            break;
        case EspecialidadAPI::API_ACTION:
            $api = new EspecialidadAPI();
            break;
        case EspecialidadxProfesionalAPI::API_ACTION:
            $api = new EspecialidadxProfesionalAPI();
            break;
        case EventoTipoAPI::API_ACTION:
            $api = new EventoTipoAPI();
            break;
        case FiltroUsadoAPI::API_ACTION:
            $api = new FiltroUsadoAPI();
            break;
        case FraseProfesionalAPI::API_ACTION:
            $api = new FraseProfesionalAPI();
            break;
        case FraseProfesionalAPI::API_ACTION:
            $api = new FraseProfesionalAPI();
            break;
        case FraseProfesionalAPI::API_ACTION:
            $api = new FraseProfesionalAPI();
            break;
        case FuncionAPI::API_ACTION:
            $api = new FuncionAPI();
            break;
        case FuncionXPremiumAPI::API_ACTION:
            $api = new FuncionXPremiumAPI();
            break;
        case PaisAPI::API_ACTION:
            $api = new PaisAPI();
            break;
        case PremiumAPI::API_ACTION:
            $api = new PremiumAPI();
            break;
        case ProblemaEspecificoAPI::API_ACTION:
            $api = new ProblemaEspecificoAPI();
            break;
        case ProblemaespecificoxProfesionalAPI::API_ACTION:
            $api = new ProblemaespecificoxProfesionalAPI();
            break;
        case ProblemaTipoAPI::API_ACTION:
            $api = new ProblemaTipoAPI();
            break;
        case ProvinciaAPI::API_ACTION:
            $api = new ProvinciaAPI();
            break;
        case RecursoAPI::API_ACTION:
            $api = new RecursoAPI();
            break;
        case TerminoCondicionAPI::API_ACTION:
            $api = new TerminoCondicionAPI();
            break;
        case EncuestaAPI::API_ACTION:
            $api = new EncuestaAPI();
            break;
        case EncuestaTipoAPI::API_ACTION:
            $api = new EncuestaTipoAPI();
            break;
        case EncuestaXSesionAPI::API_ACTION:
            $api = new EncuestaXSesionAPI();
            break;
        case EventoAPI::API_ACTION:
            $api = new EventoAPI();
            break;
        case MensajeAPI::API_ACTION:
            $api = new MensajeAPI();
            break;
        case PacienteAPI::API_ACTION:
            $api = new PacienteAPI();
            break;
        case PagoProfesionalAPI::API_ACTION:
            $api = new PagoProfesionalAPI();
            break;
        case PremiumxProfesionalAPI::API_ACTION:
            $api = new PremiumxProfesionalAPI();
            break;
        case PremiumxProfesionalAPI::API_ACTION:
            $api = new PremiumxProfesionalAPI();
            break;
        case ProblemaAPI::API_ACTION:
            $api = new ProblemaAPI();
            break;
        case ProfesionalAPI::API_ACTION:
            $api = new ProfesionalAPI();
            break;
        case SesionAPI::API_ACTION:
            $api = new SesionAPI();
            break;
        case SexoAPI::API_ACTION:
            $api = new SexoAPI();
            break;
        case TipoProblemaEspecificoAPI::API_ACTION:
            $api = new TipoProblemaEspecificoAPI();
            break;
        case ContactoWebAPI::API_ACTION:
            $api = new ContactoWebAPI();
            break;
        case ConfiguracionAPI::API_ACTION:
            $api = new ConfiguracionAPI();
            break;
        
        case UploadFileService::SERVICE_ACTION:
            $service = new UploadFileService();
            break;
        default:
            echo 'METODO NO SOPORTADO';
            break;
    }
    
    if($api != NULL) {
        $api->API();
    }
    
    if($service != NULL) {
        $service->Service();
    }
