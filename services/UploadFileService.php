<?php

class UploadFileService extends EntityAPI {
    const SERVICE_ACTION = 'uploadfile';
    private $rootPath = '';
    
    public function __construct() {
        $ini = parse_ini_file('conf.ini', true);
        if($ini['enviroment']['prod']==1) {
            $conf=$ini['prod'];
        } else {
            $conf=$ini['debug'];
        }
        $this->rootPath = $conf['images'];
    }
    
    public function Service(){
        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        $method = $_SERVER['REQUEST_METHOD'];
        
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->processDelete();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function processPost() {
        var_dump($this->rootPath);
        if (isset($_FILES['file'])) {
            $originalName = $_FILES['file']['name'];
            $ext = '.' . pathinfo($originalName, PATHINFO_EXTENSION);
            $generatedName = md5($_FILES['file']['tmp_name']) . $ext;
            $filePath = $this->rootPath . $generatedName;

            if (!is_writable($this->rootPath)) {
                $this->response(500,"false", "Directorio destino no habilitado para escritura");
                exit;
            }

            if (move_uploaded_file($_FILES['file']['tmp_name'], $filePath)) {
                $this->response(200, "true", $generatedName);
            }
        }
        else {
            $this->response(500, "false", "Archivo no subido!!");
        }
    }
    
    function response($code=200, $status="", $message="") {
        http_response_code($code);
        if( !empty($status) && !empty($message) ){
            $response = array("status" => $status ,"message"=>$message);  
            echo json_encode($response,JSON_PRETTY_PRINT);    
        }  
    }
}

